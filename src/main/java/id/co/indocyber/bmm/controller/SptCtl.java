package id.co.indocyber.bmm.controller;

import java.util.HashMap;

import id.co.indocyber.bmm.common.RestResponse;
import id.co.indocyber.bmm.service.MobSptSvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SptCtl {

	@Autowired
	MobSptSvc mobSptSvc;
	
	@RequestMapping(value = "/getSPT", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> getSPT(@RequestHeader String username,@RequestHeader String status){
		HashMap<String, Object> result = mobSptSvc.findSpt(username, status);
		RestResponse response = new RestResponse((int)result.get("status"), (String)result.get("message"), result.get("object"));
		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response,HttpStatus.OK);
		return responseEntity;
	}
	
	@RequestMapping(value = "/editSPT", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> editSPT(@RequestHeader Integer sptNo ,@RequestHeader String longitude, @RequestHeader String latitude){
		HashMap<String, Object> result = mobSptSvc.editSpt(sptNo, longitude, latitude);
		RestResponse response = new RestResponse((int)result.get("status"), (String)result.get("message"), result.get("object"));
		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response,HttpStatus.OK);
		return responseEntity;
	}
	@RequestMapping(value = "/getSptApi", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> getSPTApi(@RequestHeader String noSpt){
		HashMap<String, Object> result = mobSptSvc.tesScheduler(noSpt);
		RestResponse response = new RestResponse((int)result.get("status"), (String)result.get("message"), result.get("object"));
		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response,HttpStatus.OK);
		return responseEntity;
	}
	@RequestMapping(value = "/tesNotif", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> tesNotif(@RequestHeader String noHp){
		mobSptSvc.getNotification(noHp);;
		return null;
	}
}
