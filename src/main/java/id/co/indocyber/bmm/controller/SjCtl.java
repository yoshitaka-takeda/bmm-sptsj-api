package id.co.indocyber.bmm.controller;

import id.co.indocyber.bmm.common.RestResponse;
import id.co.indocyber.bmm.dto.FormSjEditDto;
import id.co.indocyber.bmm.dto.SjDepartingDto;
import id.co.indocyber.bmm.dto.SjFormDto;
import id.co.indocyber.bmm.service.MobSjSvc;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SjCtl {

	@Autowired 
	MobSjSvc mobSjSvc; 
	
	@RequestMapping(value = "/getSj", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> getSj(@RequestHeader String userName, @RequestHeader String status)
	{
		HashMap<String, Object> result = mobSjSvc.getSj(userName, status);
		RestResponse response = new RestResponse((int)result.get("status"), (String)result.get("message"), result.get("object"));
		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response,HttpStatus.OK);
		return responseEntity;
		
	}
	
	@RequestMapping(value = "/searchSjWithDate", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> getSjWithDate(@RequestBody SjFormDto sjFormDto )
	{
		HashMap<String, Object> result = mobSjSvc.searchSj(sjFormDto.getUserName(), sjFormDto.getDateTo(), sjFormDto.getDateFrom());
		RestResponse response = new RestResponse((int)result.get("status"), (String)result.get("message"), result.get("object"));
		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response,HttpStatus.OK);
		return responseEntity;
		
	}
	
//	@RequestMapping(value = "/editSj", method = RequestMethod.POST)
//	public ResponseEntity<RestResponse> editSj(@RequestHeader String userName, @RequestHeader String longitude, @RequestHeader String latitude )
//	{
//		HashMap<String, Object> result = mobSjSvc.editSj(userName,latitude,longitude);
//		RestResponse response = new RestResponse((int)result.get("status"), (String)result.get("message"), result.get("object"));
//		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response,HttpStatus.OK);
//		return responseEntity;
//		
//	}
	
	@RequestMapping(value = "/sjAndGoodsReceive", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> sjAndGoodsReceive(@RequestBody FormSjEditDto formSjEditDto )
	{
		HashMap<String, Object> result = mobSjSvc.editSjAndGoodReceive(formSjEditDto);
		RestResponse response = new RestResponse((int)result.get("status"), (String)result.get("message"), result.get("object"));
		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response,HttpStatus.OK);
		return responseEntity;
		
	}
	
	@RequestMapping(value = "/departingSJ", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> departingSj(@RequestBody SjDepartingDto departingDto )
	{
		HashMap<String, Object> result = mobSjSvc.departing(departingDto);
		RestResponse response = new RestResponse((int)result.get("status"), (String)result.get("message"), result.get("object"));
		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response,HttpStatus.OK);
		return responseEntity;
		
	}
	
	@RequestMapping(value = "/pinValidation", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> pinValidation(@RequestHeader String pin,@RequestHeader Integer sjNo )
	{
		HashMap<String, Object> result = mobSjSvc.pinValidation(pin, sjNo);
		RestResponse response = new RestResponse((int)result.get("status"), (String)result.get("message"), result.get("object"));
		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response,HttpStatus.OK);
		return responseEntity;
		
	}

}
