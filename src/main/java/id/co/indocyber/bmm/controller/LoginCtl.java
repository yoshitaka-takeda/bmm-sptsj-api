package id.co.indocyber.bmm.controller;

import java.util.HashMap;

import id.co.indocyber.bmm.common.RestResponse;
import id.co.indocyber.bmm.dto.ChangePassDto;
import id.co.indocyber.bmm.dto.LoginDto;
import id.co.indocyber.bmm.dto.UserLoginXmlDto;
import id.co.indocyber.bmm.service.MUserSvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginCtl {

	@Autowired
	MUserSvc mUserSvc;
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> selectMenu(@RequestBody LoginDto loginDto){
		HashMap<String, Object> result = mUserSvc.login2(loginDto);
		RestResponse response = new RestResponse((int)result.get("status"), (String)result.get("message"), result.get("object"));
		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response,HttpStatus.OK);
		return responseEntity;
		
	}
	
//	@RequestMapping(value = "/loginXml", method = RequestMethod.POST)
//	public ResponseEntity<UserLoginXmlDto> selectMenuXml(@RequestBody LoginDto loginDto){
//		HashMap<String, Object> result = mUserSvc.loginXml(loginDto);
//		//RestResponse response = new RestResponse((int)result.get("status"), (String)result.get("message"), result.get("object"));
//		ResponseEntity<UserLoginXmlDto> responseEntity = new ResponseEntity<UserLoginXmlDto>((UserLoginXmlDto) result.get("object"),HttpStatus.OK);
//		return responseEntity;
//		
//	}
	
	@RequestMapping(value = "/changePassword", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> changePass(@RequestBody ChangePassDto changePassDto){
		HashMap<String, Object> result = mUserSvc.changePassword(changePassDto);
		RestResponse response = new RestResponse((int)result.get("status"), (String)result.get("message"), result.get("object"));
		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response,HttpStatus.OK);
		return responseEntity;
		
	}
	
	@RequestMapping(value = "/registerImei", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> regImei(@RequestHeader String email, @RequestHeader String imei){
		HashMap<String, Object> result = mUserSvc.registerImei(email, imei);
		RestResponse response = new RestResponse((int)result.get("status"), (String)result.get("message"), result.get("object"));
		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response,HttpStatus.OK);
		return responseEntity;
	}
	
	@RequestMapping(value = "/getNoHp", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> getNoHP(@RequestHeader String imei){
		HashMap<String, Object> result = mUserSvc.getNoHp(imei);
		RestResponse response = new RestResponse((int)result.get("status"), (String)result.get("message"), result.get("object"));
		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response,HttpStatus.OK);
		return responseEntity;
		
	}
}
