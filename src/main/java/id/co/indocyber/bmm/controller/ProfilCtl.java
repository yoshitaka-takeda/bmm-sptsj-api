package id.co.indocyber.bmm.controller;

import java.util.HashMap;

import id.co.indocyber.bmm.common.RestResponse;
import id.co.indocyber.bmm.dto.UserEditForm;
import id.co.indocyber.bmm.dto.UserProfileDto;
import id.co.indocyber.bmm.service.MobDriverSvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProfilCtl {

	@Autowired
	MobDriverSvc mobDriverSvc;
	
	@RequestMapping(value = "/getProfile", method = RequestMethod.GET)
	public ResponseEntity<RestResponse> getProfile(@RequestHeader String email){
		HashMap<String, Object> result = mobDriverSvc.profileDriver(email);
		RestResponse response = new RestResponse((int)result.get("status"), (String)result.get("message"), result.get("object"));
		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response,HttpStatus.OK);
		return responseEntity;
		
	}
	
	@RequestMapping(value = "/editProfile", method = RequestMethod.POST)
	public ResponseEntity<RestResponse> editProfile(@RequestHeader String email, @RequestBody UserProfileDto userProfileDto){
		HashMap<String, Object> result = mobDriverSvc.updateProfile(userProfileDto, email);
		RestResponse response = new RestResponse((int)result.get("status"), (String)result.get("message"), result.get("object"));
		ResponseEntity<RestResponse> responseEntity = new ResponseEntity<>(response,HttpStatus.OK);
		return responseEntity;
		
	}
}
