package id.co.indocyber.bmm.service;

import id.co.indocyber.bmm.dto.MobSptDto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

public interface MobSptSvc {

	public HashMap<String, Object> findSpt(String username, String status);
	//public HashMap<String, Object> getdSptDetail(String username, String status);
	public HashMap<String, Object> editSpt(Integer sptNo, String longitude, String latitude);
	public HashMap<String, Object> tesScheduler(String SptNo);
	public void getNotification(String noHp);
	
}
