package id.co.indocyber.bmm.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import ma.glasnost.orika.MapperFacade;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

import id.co.indocyber.bmm.common.AttributeParam;
import id.co.indocyber.bmm.config.ConfigFunction;
import id.co.indocyber.bmm.dao.MobDriverDao;
import id.co.indocyber.bmm.dao.MobGoodReceiveAttachmentDao;
import id.co.indocyber.bmm.dao.MobGoodReceiveDao;
import id.co.indocyber.bmm.dao.MobSjDao;
import id.co.indocyber.bmm.dao.MobSptDao;
import id.co.indocyber.bmm.dto.FormSjEditDto;
import id.co.indocyber.bmm.dto.MobSjDto;
import id.co.indocyber.bmm.dto.SjDepartingDto;
import id.co.indocyber.bmm.dto.SjDto;
import id.co.indocyber.bmm.entity.MobGoodsReceive;
import id.co.indocyber.bmm.entity.MobGoodsReceiveAttachment;
import id.co.indocyber.bmm.entity.MobSj;
import id.co.indocyber.bmm.entity.MobSpt;
import id.co.indocyber.bmm.service.MobSjSvc;

@Service("mobSjSvc")
@Transactional
public class MobSjSvcImpl implements MobSjSvc {

	@Autowired
	MobSjDao mobSjDao;
	@Autowired
	MobSptDao mobSptDao;
	@Autowired
	MobDriverDao mobDriverDao;
	@Autowired
	MobGoodReceiveDao goodsReceivedao;
	@Autowired
	MobGoodReceiveAttachmentDao attachmentDao;
	@Autowired
	MapperFacade mapperFacade;
		
	SimpleDateFormat a = new SimpleDateFormat("dd/MM/yyyy");
	ConfigFunction saveImage= new ConfigFunction();
	
	
	@Override
	public HashMap<String, Object> getSj(String userName, String status) {		
		HashMap<String, Object> result = new HashMap<>();		
		
		List<Object[]> listobjSj = new ArrayList<>();
		
			for (Object[] objects : mobSjDao.findSJ(userName, status)) {
				listobjSj.add(objects);
			}
			for (Object[] objects : mobSjDao.findSJDriver2(userName, status)) {
				listobjSj.add(objects);
			}
			if(!listobjSj.isEmpty()){
				List<SjDto> lisMobSjDtos =  new ArrayList<SjDto>();
				for (Object[] o : listobjSj) {
					SjDto mobSjDto = new SjDto();
					mobSjDto.setNoSj((Integer)o[0]);
					mobSjDto.setTgl(a.format((Date)o[1]));
					String cust=(String) o[2];
					if(cust!=null){

						mobSjDto.setCustomer(cust.trim());
					}
					mobSjDto.setNoSpt((Integer)o[3]);
					String addrShipTo=(String) o[4];
					if(addrShipTo!=null){

						mobSjDto.setAddressShipTo(addrShipTo.trim());
					}
					mobSjDto.setNoVehicle((String)o[5]);
					mobSjDto.setNoSppb((Integer)o[6]);
					mobSjDto.setNoSegel((String)o[7]+"-"+(String)o[8]);
					mobSjDto.setItemName((String)o[9]);
					mobSjDto.setQty((BigDecimal)o[11]==null?0d:((BigDecimal)o[11]).doubleValue()/50);
					mobSjDto.setWeight((BigDecimal)o[11]==null?0d:((BigDecimal)o[11]).doubleValue());
					try{
					mobSjDto.setIsTransit((Byte)o[12]==0?false:true);
					}catch(Exception e){
						try{
						mobSjDto.setIsTransit(((BigInteger)o[12]).intValue()==0?false:true);
						}catch(Exception E){
							mobSjDto.setIsTransit(((Integer)o[12])==0?false:true);
						}
					}
					try{
					mobSjDto.setIsDepart(((Integer)o[13])==0?false:true);
					}catch(Exception e){
						mobSjDto.setIsDepart(((BigInteger)o[13]).intValue()==0?false:true);
					}
					mobSjDto.setContainerNo((String)o[14]);
					lisMobSjDtos.add(mobSjDto);
				}
				result.put("status", 1);
				result.put("message", "success");
				result.put("object", lisMobSjDtos);
			}else{
				result.put("status", 0);
				result.put("message", "data tidak ada");
				result.put("object", null);
			}
			
		
		return result;
	}

//	@Override
//	public HashMap<String, Object> editSj(Integer sjNo, String latitude,String longitude) {
//
//		HashMap<String, Object> result = new HashMap<>();
//		MobSj mobSj = mobSjDao.findOne(sjNo);
//		
//		try{
//			mobSjDao.save(mobSj);
//			result.put("status", 1);
//			result.put("message", "success");
//			result.put("object", null);
//		}catch(Exception e){
//			result.put("status", 0);
//			result.put("message", "gagal update data");
//			result.put("object", null);
//		}
//		return result;
//	}

	@Override
	public HashMap<String, Object> searchSj(String userName, String dateTo,
			String dateFrom) {
		List<Object[]> listobjSj = new ArrayList<>();

		try{
			dateFrom=dateFrom+" 00:00:00";
			dateTo=dateTo+" 23:59:59";
			Date date1=dateFrom.equals("")?new Date(0):new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(dateFrom);
			Date date2=dateTo.equals("")?new Date():new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").parse(dateTo);
			listobjSj = mobSjDao.findSJWithDate(userName, date1,date2 );
		}catch(Exception e){
			e.printStackTrace();
		}
		
		List<SjDto> lisMobSjDtos =  new ArrayList<SjDto>();
		HashMap<String, Object> result = new HashMap<>();
		
		if(listobjSj.isEmpty()){
			result.put("status", 0);
			result.put("message", "data tidak ada");
			result.put("object", null);
		}else{
			for (Object[] o : listobjSj) {
				SjDto mobSjDto = new SjDto();
				mobSjDto.setNoSj((Integer)o[0]);
				mobSjDto.setTgl(a.format((Date)o[1]));
				String cust=(String) o[2];
				if(cust!=null){

					mobSjDto.setCustomer(cust.trim());
				}
				mobSjDto.setNoSpt((Integer)o[3]);
				String addrShipTo=(String) o[4];
				if(addrShipTo!=null){

					mobSjDto.setAddressShipTo(addrShipTo.trim());
				}
				mobSjDto.setNoVehicle((String)o[5]);
				mobSjDto.setNoSppb((Integer)o[6]);
				mobSjDto.setNoSegel((String)o[7]+"-"+(String)o[8]);
				mobSjDto.setItemName((String)o[9]);
				mobSjDto.setWeight((BigDecimal)o[11]==null?0d:((BigDecimal)o[11]).doubleValue());
				mobSjDto.setQty((BigDecimal)o[11]==null?0d:((BigDecimal)o[11]).doubleValue()/50);
				mobSjDto.setIsTransit((Byte)o[12]==0||((Byte)o[12]).equals(null)?false:true);
				try{
				mobSjDto.setIsDepart(((BigInteger)o[15])==null||((BigInteger)o[15]).intValue()==0?false:true);
				}catch(Exception e){
					mobSjDto.setIsDepart(((Integer)o[15])==null||((Integer)o[15])==0?false:true);
				}
				mobSjDto.setQtyReceive(((BigDecimal)o[13])==null?0d:((BigDecimal)o[13]).doubleValue());
				mobSjDto.setNotes((String)o[14]);
				mobSjDto.setContainerNo((String)o[16]);
				
				lisMobSjDtos.add(mobSjDto);
			}
			result.put("status", 1);
			result.put("message", "success");
			result.put("object", lisMobSjDtos);
		}
		return result;
	}

	@Override
	public HashMap<String, Object> editSjAndGoodReceive(FormSjEditDto editDto) {
		HashMap<String, Object> result = new HashMap<>();
		List<Object[]> objSJ= mobSjDao.findSJWithNo(editDto.getNoSj());
		
		if(objSJ.isEmpty()){
			result.put("status", 0);
			result.put("message", "data kosong");
			result.put("object", null);
			
			return result;
		}
		MobSj mobSj = new MobSj();
		MobSjDto mobSjDto = new MobSjDto();
		for (Object[] o : objSJ) {
			mobSj = (MobSj)o[0];
			mobSjDto = mapperFacade.map(mobSj, MobSjDto.class);
			mobSjDto.setIsTransit((Boolean)o[1]);
		}
		MobGoodsReceive mobGoodsReceive = new MobGoodsReceive();
		MobSpt mobSpt = mobSptDao.findSptbyNo(mobSj.getSptNo());
		if(!mobSjDto.getIsTransit()){
			
			mobSj.setDestinationLatitude(Double.parseDouble(editDto.getDestinationLatitude()));
			mobSj.setDestinationLongitude(Double.parseDouble(editDto.getDestinationLongitude()));
			mobSj.setDestinationBy(editDto.getUserNameDriver());
			mobSj.setDestinationDate(new Date());
			mobSj.setStatus("C");
			mobSj.setModified(editDto.getUserNameDriver());
			mobSj.setModifiedDate(new Date());
			mobSjDao.save(mobSj);
			
			mobSpt.setModified(editDto.getUserNameDriver());;
			mobSpt.setStatus("C");
			mobSpt.setModifiedDate(new Date());
			mobSptDao.save(mobSpt);
			
			mobGoodsReceive.setDriverId(mobSj.getDriverId());
			mobGoodsReceive.setCreated(editDto.getUserNameDriver());
			mobGoodsReceive.setModified(editDto.getUserNameDriver());
			mobGoodsReceive.setCreatedDate(new Date());
			mobGoodsReceive.setModifiedDate(new Date());
			mobGoodsReceive.setSjNo(mobSj.getSjNo());
			mobGoodsReceive.setQtyReceive(new Double(editDto.getQtyReceive()));
			mobGoodsReceive.setNotes(editDto.getNotes());
			mobGoodsReceive.setNameOfRecipient(editDto.getRecipientName());
			mobGoodsReceive.setVehicleNo(mobSj.getVehicleNo());
			mobGoodsReceive.setVehicleType(mobSj.getVehicleType());
			mobGoodsReceive.setSignature(AttributeParam.host+saveImage.SaveBase64toImage(editDto.getSignature(),AttributeParam.folderNameSignature,1).substring(19).replace("\\", "/"));
			mobGoodsReceive.setDestinationLatitude(Double.parseDouble(editDto.getDestinationLatitude()));
			mobGoodsReceive.setDestinationLongitude(Double.parseDouble(editDto.getDestinationLongitude()));
			goodsReceivedao.save(mobGoodsReceive);
			result = saveAttachment(editDto);
			
			//set status ke SAP
			setApiSj(mobSj.getSjNo(),mobGoodsReceive.getQtyReceive(),mobGoodsReceive.getNotes(), mobGoodsReceive.getSignature(), mobGoodsReceive.getNameOfRecipient());
			
		}else if(mobSjDto.getIsTransit() && mobSjDto.getTransitLatitude()==null && mobSjDto.getTransitLongitude()==null){
			
			mobSj.setModified(editDto.getUserNameDriver());
			mobSj.setFirstDestinationLatitude(Double.parseDouble(editDto.getDestinationLatitude()));
			mobSj.setFirstDestinationLongitude(Double.parseDouble(editDto.getDestinationLongitude()));
			mobSj.setFirstDestinationBy(editDto.getUserNameDriver());
			mobSj.setStatus("R");
			mobSj.setFirstDestinationDate(new Date());
			mobSjDao.save(mobSj);
			result.put("status", 1);
			result.put("message", "sukses");
			result.put("object", editDto);
			
		}else{
			mobSj.setDestinationLatitude(Double.parseDouble(editDto.getDestinationLatitude()));
			mobSj.setDestinationLongitude(Double.parseDouble(editDto.getDestinationLongitude()));
			mobSj.setDestinationBy(editDto.getUserNameDriver());
			mobSj.setDestinationDate(new Date());
			mobSj.setStatus("C");
			mobSj.setModified(editDto.getUserNameDriver());
			mobSjDao.save(mobSj);
			
			mobSpt.setStatus("C");
			mobSptDao.save(mobSpt);
			
			mobGoodsReceive= goodsReceivedao.findGoodReceivebySjno(editDto.getNoSj());
			mobGoodsReceive.setModified(editDto.getUserNameDriver());
			mobGoodsReceive.setModifiedDate(new Date());
			mobGoodsReceive.setQtyReceive(new Double(editDto.getQtyReceive()));
			mobGoodsReceive.setNotes(editDto.getNotes());
			mobGoodsReceive.setNameOfRecipient(editDto.getRecipientName());
			mobGoodsReceive.setVehicleNo(mobSj.getVehicleNo());
			mobGoodsReceive.setSignature(AttributeParam.host+saveImage.SaveBase64toImage(editDto.getSignature(),AttributeParam.folderNameSignature,1).substring(19).replace("\\", "/"));
			mobGoodsReceive.setDestinationLatitude(Double.parseDouble(editDto.getDestinationLatitude()));
			mobGoodsReceive.setDestinationLongitude(Double.parseDouble(editDto.getDestinationLongitude()));
			goodsReceivedao.save(mobGoodsReceive);
			
			result = saveAttachment(editDto);
			
			//set status ke SAP
			setApiSj(mobSj.getSjNo(),mobGoodsReceive.getQtyReceive(),mobGoodsReceive.getNotes(), mobGoodsReceive.getSignature(), mobGoodsReceive.getNameOfRecipient());
		}
		
		return result;
	}
	
	

	@Override
	public HashMap<String, Object> pinValidation(String pin, Integer sjNo) {
		HashMap<String, Object> result = new HashMap<>();
		MobSj mobSj = mobSjDao.findOne(sjNo);
		if (mobSj.getPin().toString().equals(pin)) {
			result.put("status", 1);
			result.put("message", "sukses");
			result.put("object", null);
		}else {
			result.put("status", 0);
			result.put("message", "pin salah");
			result.put("object", null);
		}
		return result;
	}

	@Override
	public HashMap<String, Object> departing(SjDepartingDto departingDto) {
		HashMap<String, Object> result = new HashMap<>();
		List<Object[]> objSJ= mobSjDao.findSJWithNo(departingDto.getSjNo());
		if(objSJ.isEmpty()){
			result.put("status", 0);
			result.put("message", "data kosong");
			result.put("object", null);
			
			return result;
		}
		MobSj mobSj = new MobSj();
		MobSjDto mobSjDto = new MobSjDto();
		for (Object[] o : objSJ) {
			mobSj = (MobSj)o[0];
			mobSjDto = mapperFacade.map(mobSj, MobSjDto.class);
			mobSjDto.setIsTransit((Boolean)o[1]);
		}
		
		try{
			if(mobSjDto.getIsTransit()==true && mobSjDto.getDepartingFromLatitude()!=null && mobSjDto.getDepartingFromLongitude()!=null){
				mobSj.setTransitLatitude(Double.parseDouble(departingDto.getDepartingLatitude()));
				mobSj.setTransitLongitude(Double.parseDouble(departingDto.getDepartingLongitude()));
				mobSj.setTransitDate(new Date());
				mobSj.setTransitBy(departingDto.getUserName());
				mobSj.setModified(departingDto.getUserName());
				mobSjDao.save(mobSj);
				result.put("status", 1);
				result.put("message", "sukses");
				result.put("object", departingDto);
			}else{
				mobSj.setDepartingFromLatitude(Double.parseDouble(departingDto.getDepartingLatitude()));
				mobSj.setDepartingFromLongitude(Double.parseDouble(departingDto.getDepartingLongitude()));
				mobSj.setDepartingFromDate(new Date());
				mobSj.setDepartingFromBy(departingDto.getUserName());
				mobSj.setModified(departingDto.getUserName());
				mobSjDao.save(mobSj);
				result.put("status", 1);
				result.put("message", "sukses");
				result.put("object", departingDto);
			}
			
		}catch(Exception e){
			result.put("status", 0);
			result.put("message", "gagal");
			result.put("object", null);
		}
			
		return result;
	}
	
	
	public HashMap<String, Object> saveAttachment(FormSjEditDto editDto){
		HashMap<String, Object> result = new HashMap<>();
		List<String> a = editDto.getPhotoDoc();
		Integer i=attachmentDao.findMaxId();
		if(i==null){
			i=1;
		}else{
			i=attachmentDao.findMaxId()+1;
		}
		int seq = 1;
		for (String string : a) {
			MobGoodsReceiveAttachment attachment = new MobGoodsReceiveAttachment();
			attachment.setId(i++);
			attachment.setSjNo(editDto.getNoSj());
			if(!string.equals("")||string.length()>0){
				String path = AttributeParam.host+saveImage.SaveBase64toImage(string,AttributeParam.folderNameAttachment, seq).substring(19).replace("\\", "/");
				attachment.setDocumentPath(path);
				attachment.setCreated(editDto.getUserNameDriver());
				attachment.setDocumentBlob(string);
				attachment.setCreatedDate(new Date());
				attachmentDao.save(attachment);
				seq++;
			}
		}
		try {	
			
			result.put("status", 1);
			result.put("message", "sukses");
			result.put("object", editDto);
		} catch (Exception e) {
			e.printStackTrace();
			result.put("status", 0);
			result.put("message", "gagal menyimpan");
			result.put("object", null);
		}
		return result;
	}
	
	public void setApiSj(Integer sjNo, Double jmlTerima,String note, String pathSignature, String namaPenerima){
		//HashMap<String, Object> result = new HashMap<>();
		HttpResponse response;
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(AttributeParam.hostToken);
		JSONObject jobj = new JSONObject();
		try {
			response = httpClient.execute(request);

			HttpEntity entity = response.getEntity();
			String responseString;
			responseString = EntityUtils.toString(entity);
			jobj = new JSONObject(responseString);
			@SuppressWarnings("unchecked")
			HashMap<String, Object> yourHashMap = new Gson().fromJson(
					jobj.get("data").toString(), HashMap.class);
			String a = yourHashMap.get("token").toString();
			
			HttpPost post = new HttpPost(AttributeParam.hostUpdateSJ);

			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			urlParameters.add(new BasicNameValuePair("token", a));
			urlParameters.add(new BasicNameValuePair("sj_no", sjNo.toString()));
			urlParameters.add(new BasicNameValuePair("jml_terima", jmlTerima.toString()));
			urlParameters.add(new BasicNameValuePair("note", note));
			urlParameters.add(new BasicNameValuePair("signature_path", pathSignature));
			urlParameters.add(new BasicNameValuePair("receiver", namaPenerima));
			

			post.setEntity(new UrlEncodedFormEntity(urlParameters));
							
			response = httpClient.execute(post);
			HttpEntity entity2 = response.getEntity();
			String responseString2 = EntityUtils.toString(entity2);
			
			jobj = new JSONObject(responseString2);
			@SuppressWarnings("unchecked")
			HashMap<String, Object> yourHashMapstatus = new Gson().fromJson(
					jobj.get("data").toString(), HashMap.class);
			String sts = yourHashMapstatus.get("data").toString();
			System.out.println("Status update Sj : "+sts);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
