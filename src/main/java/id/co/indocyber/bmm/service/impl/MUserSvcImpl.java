package id.co.indocyber.bmm.service.impl;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;

import id.co.indocyber.bmm.dao.MUserDao;
import id.co.indocyber.bmm.dto.ChangePassDto;
import id.co.indocyber.bmm.dto.LoginDto;
import id.co.indocyber.bmm.dto.UserLoginDto;
import id.co.indocyber.bmm.dto.UserLoginXmlDto;
import id.co.indocyber.bmm.entity.MUser;
import id.co.indocyber.bmm.entity.MUserPK;
import id.co.indocyber.bmm.service.MUserSvc;
import io.grpc.netty.shaded.io.netty.util.internal.StringUtil;

@Service("mUserSvc")
@Transactional
public class MUserSvcImpl implements MUserSvc {

	@Autowired
	MUserDao mUserDao;

	String msg = "";

	public String hash(String username, String pass) {
		String toHash = username + "berkah" + pass + "manisindo";
		return DigestUtils.sha1Hex(toHash);
	}

	@Override
	public HashMap<String, Object> login(LoginDto loginDto) {

		HashMap<String, Object> result = new HashMap<>();
		MUser a = mUserDao.getUserByUsername(loginDto.getUsername());
		if (a == null) {
			msg = "username atau password salah";
			result.put("status", 0);
			result.put("message", msg);
			result.put("object", null);
			return result;
		}
		String pass = hash(a.getUsername(), loginDto.getPass());
		List<Object[]> objUserLogin = mUserDao.findUserLogina(a.getUsername(),
				pass);
		UserLoginDto userLoginDto = new UserLoginDto();
		MUser mUser = new MUser();
		Boolean isActiveUser = null, isActiveUserRole = null, isActiveRole = null, isActiveMapping = null, isActiveTransportir = null;
		if (objUserLogin.isEmpty()) {
			msg = "username atau password salah";
			result.put("status", 0);
			result.put("message", msg);
			result.put("object", null);
		} else {
			for (Object[] o : objUserLogin) {
				mUser = (MUser) o[0];
				userLoginDto.setRoleId((Integer) o[1]);
				userLoginDto.setPersonId((Integer) o[2]);
				userLoginDto.setRoleName((String) o[3]);
				userLoginDto.setFullname((String) o[4]);
				userLoginDto.setEmail((String) o[5]);
				userLoginDto.setToken((String) o[6]);
				userLoginDto.setUserName((String) o[7]);
				userLoginDto.setPhotoText((String) o[8]);
				isActiveUser = (Boolean) o[9];
				isActiveUserRole = (Byte) o[10] != 0;
				isActiveRole = (Byte) o[11] != 0;
				isActiveMapping = (Boolean) o[12];
				isActiveTransportir = (Boolean) o[13];
			}
			// if(countInv<maxInv){
			if (checkUserName(mUser, loginDto.getUsername())) {
				HashMap<String, Object> map = checkPassword(mUser, pass);
				if (map.get("status").equals(1)) {
					if (mUser.getImei() == null) {
						result.put("status", 0);
						result.put("message", "harap register imei");
						result.put("object", userLoginDto);
					} else if (checkImei(mUser.getImei(), loginDto.getImei())) {
						if (checkActive(isActiveUser, isActiveUserRole,
								isActiveRole, isActiveMapping,
								isActiveTransportir)) {
							mUser.setToken(loginDto.getToken());
							mUserDao.save(mUser);
							result.put("status", 1);
							result.put("message", msg);
							result.put("object", userLoginDto);
						} else {
							result.put("status", 0);
							result.put("message", msg);
							result.put("object", null);
						}
					} else {
						result.put("status", 0);
						result.put("message", "Imei tidak terdaftar");
						result.put("object", null);
					}
				} else if (map.get("status").equals(2)) {
					// String x= mUser.getImei();
					// if(x!=null){
					// result.put("status", 0);
					// result.put("message",
					// "silahkan hubungi admin untuk reset Imei");
					// }else{
					result.put("status", map.get("status"));
					result.put("message", map.get("message"));
					result.put("object", userLoginDto);
					// }
				} else {
					// countInv++;
					result.put("status", map.get("status"));
					result.put("message", map.get("message"));
					result.put("object", null);
				}
			} else {
				result.put("status", 0);
				result.put("message", "username atau password salah");
				result.put("object", null);
			}
		}
		return result;
	}

	private Boolean checkUserName(MUser mUser, String usernameInput) {
		if (mUser.getEmail().equals(usernameInput)) {
			return true;
		} else if (mUser.getUsername().equals(usernameInput)) {
			return true;
		} else {
			return false;
		}
	}

	private HashMap<String, Object> checkPassword(MUser mUser, String passInput) {
		HashMap<String, Object> result = new HashMap<>();
		// String x= mUser.getImei();
		// if(x!=null || !mUser.getImei().equals("")){
		// result.put("status", 0);
		// result.put("message", "silahkan hubungi admin untuk reset Imei");
		// }else{
		if (mUser.getPassword().equals(mUser.getPswdDefault())) {
			result.put("status", 2);
			result.put("message", "change password");
		} else if (passInput.equals(mUser.getPassword())) {
			result.put("status", 1);
			result.put("message", "sukses");
		} else {
			result.put("status", 0);
			result.put("message", "username atau password salah");
		}
		// }
		return result;
	}

	private Boolean checkImei(String imei, String imeiInput) {
		if (imeiInput.equals(imei)) {
			return true;
		} else {
			return false;
		}
	}

	private Boolean checkActive(Boolean isActiveUser, Boolean isActiveUserRole,
			Boolean isActiveRole, Boolean isActiveMapping,
			Boolean isActiveTransportir) {
		if (isActiveUser == true) {
			if (isActiveUserRole == true) {
				if (isActiveRole == true) {
					if (isActiveMapping == true) {
						if (isActiveTransportir == true) {
							msg = "berhasil login";
							return true;
						} else {
							msg = "Transportir tidak aktif";
							return false;
						}
					} else {
						msg = "user tidak aktif";
						return false;
					}

				} else {
					msg = "role tidak aktif";
					return false;
				}
			} else {
				msg = "user role tidak aktif";
				return false;
			}
		} else {
			msg = "user tidak aktif";
			return false;
		}
	}

	@Override
	public HashMap<String, Object> changePassword(ChangePassDto changePassDto) {
		HashMap<String, Object> map = new HashMap<>();
		MUser mUser = mUserDao.getUserByUsername(changePassDto.getUsername());
		String pass = hash(mUser.getUsername(), changePassDto.getOldpass());
		String passNew = hash(mUser.getUsername(), changePassDto.getNewpass());
		if(passNew.equals(mUser.getPswdDefault())){
			map.put("status", 0);
			map.put("message", "Tidak dapat menggunakan password default");
			map.put("object", null);
		}
		 else if (mUser.getPassword().equals(passNew)) {
				map.put("status", 0);
				map.put("message", "Password anda yang baru sama dengan password yang lama");
				map.put("object", null);
			}
		else if (mUser.getPassword().equals(pass)) {
			// List<String> imei = mUserDao.listIMei(loginDto.getImei());
			// if(imei.size()==0){
			mUser.setPassword(passNew);
			// mUser.setImei(loginDto.getImei());
			// mUser.setToken(changePassDto.getToken());
			try {
				mUserDao.save(mUser);
				map.put("status", 1);
				map.put("message", "sukses ganti password");
				map.put("object", null);
			} catch (Exception e) {
				map.put("status", 0);
				map.put("message", "gagal update password");
				map.put("object", null);
				e.printStackTrace();
			}
			// }else{
			// map.put("status", 0);
			// map.put("message", "imei sudah terdaftar");
			// map.put("object", null);
			// }
		} 
		else{
			map.put("status", 0);
			map.put("message", "gagal update password");
			map.put("object", null);
		}
		return map;
	}

	@Override
	public HashMap<String, Object> loginXml(LoginDto loginDto) {
		// TODO Auto-generated method stub

		List<Object[]> objUserLogin = mUserDao.findUserLogina(
				loginDto.getUsername(), loginDto.getPass());
		UserLoginXmlDto userLoginDto = new UserLoginXmlDto();
		HashMap<String, Object> result = new HashMap<>();

		for (Object[] o : objUserLogin) {
			// mUser = (MUser)o[0];
			userLoginDto.setRoleId((Integer) o[1]);
			userLoginDto.setPersonId((Integer) o[2]);
			userLoginDto.setRoleName((String) o[3]);
			userLoginDto.setFullname((String) o[4]);
			userLoginDto.setEmail((String) o[5]);
			userLoginDto.setToken((String) o[6]);
			userLoginDto.setUserName((String) o[7]);
			userLoginDto.setPhotoText((String) o[8]);

		}
		result.put("status", 1);
		result.put("message", msg);
		result.put("object", userLoginDto);

		return result;
	}

	@Override
	public HashMap<String, Object> registerImei(String email, String imei) {
		HashMap<String, Object> result = new HashMap<>();
		List<String> listImei = mUserDao.listIMei(imei);
		if (listImei.size() == 0) {
//			MUserPK mUserPK = new MUserPK();
//			mUserPK.setEmail(email);
			MUser mUser = mUserDao.getUserByUsername(email);
			if(mUser!=null){
				

				if(StringUtil.isNullOrEmpty(mUser.getImei())){
				mUser.setImei(imei);
				mUserDao.save(mUser);				
				result.put("status", 1);
				result.put("message", "Sukses register");
				result.put("object", null);
				}else{
					result.put("status", 0);
					result.put("message", "Imei di no handphone ini telah terdaftar di user lain");						
					result.put("object", null);
				}
			}else{
				result.put("status", 0);
				result.put("message", "Data tidak ditemukan");
				result.put("object", null);
			}
		} else {
			result.put("status", 0);
			result.put("message", "Imei atau no handphone telah terdaftar di user lain");
			result.put("object", null);
		}

		return result;
	}

	@Override
	public HashMap<String, Object> login2(LoginDto loginDto) {
		HashMap<String, Object> result = new HashMap<>();
		MUser a = mUserDao.getUserByUsername(loginDto.getUsername());
		if (a == null) {
			msg = "username atau password salah";
			result.put("status", 0);
			result.put("message", msg);
			result.put("object", null);
			return result;
		}
		String pass = hash(a.getUsername(), loginDto.getPass());
		List<Object[]> objUserLogin = mUserDao.findUserLogina(a.getUsername(),
				pass);
		UserLoginDto userLoginDto = new UserLoginDto();
		MUser mUser = new MUser();
		Boolean isActiveUser = null, isActiveUserRole = null, isActiveRole = null, isActiveMapping = null, isActiveTransportir = null;
		if (objUserLogin.isEmpty()) {
			msg = "username atau password salah";
			result.put("status", 0);
			result.put("message", msg);
			result.put("object", null);
		} else {
			for (Object[] o : objUserLogin) {
				mUser = (MUser) o[0];
				userLoginDto.setRoleId((Integer) o[1]);
				userLoginDto.setPersonId((Integer) o[2]);
				userLoginDto.setRoleName((String) o[3]);
				userLoginDto.setFullname((String) o[4]);
				userLoginDto.setEmail((String) o[5]);
				userLoginDto.setToken((String) o[6]);
				userLoginDto.setUserName((String) o[7]);
				userLoginDto.setPhotoText((String) o[8]);
				isActiveUser = (Boolean) o[9];
				isActiveUserRole = (Byte) o[10] != 0;
				isActiveRole = (Byte) o[11] != 0;
				isActiveMapping = (Byte) o[12] !=0;
				isActiveTransportir = (Byte) o[13] !=0;
			}
			if (mUser.getImei() == null) {
				result.put("status", 0);
				result.put("message", "harap register imei");
				result.put("object", userLoginDto);
			}
			if (checkImei(mUser.getImei(), loginDto.getImei())) {
				HashMap<String, Object> map = checkPassword(mUser, pass);
				if (map.get("status").equals(1)) {
					if (checkActive(isActiveUser, isActiveUserRole,
							isActiveRole,isActiveMapping,isActiveTransportir)) {
						mUser.setToken(loginDto.getToken());
						mUserDao.save(mUser);
						result.put("status", 1);
						result.put("message", msg);
						result.put("object", userLoginDto);
					} else {
						result.put("status", 0);
						result.put("message", msg);
						result.put("object", null);
					}
				} else if (map.get("status").equals(2)) {
					result.put("status", map.get("status"));
					result.put("message", map.get("message"));
					result.put("object", userLoginDto);
				} else {
					result.put("status", map.get("status"));
					result.put("message", map.get("message"));
					result.put("object", null);
				}
			} else {
				result.put("status", 0);
				result.put("message", "Imei tidak terdaftar");
				result.put("object", null);
			}
		}
		return result;
	}

	@Override
	public HashMap<String, Object> getNoHp(String imei) {
		HashMap<String, Object> result = new HashMap<>();
		String phoneNo = mUserDao.getPhoneNum(imei);
		if(phoneNo==null){
			result.put("status", 0);
			result.put("message", "data tidak ditemukan");
			result.put("object", null);
		}else{
			result.put("status", 1);
			result.put("message", "success");
			result.put("object", phoneNo);
		}
		return result;
	}

}
