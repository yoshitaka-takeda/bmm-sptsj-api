package id.co.indocyber.bmm.service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.ls.LSInput;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.common.collect.Lists;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.google.gson.Gson;

import id.co.indocyber.bmm.common.AttributeParam;
import id.co.indocyber.bmm.common.JsonUtil;
import id.co.indocyber.bmm.dao.MobDriverDao;
import id.co.indocyber.bmm.dao.MobSptDao;
import id.co.indocyber.bmm.dto.ListDataApi;
import id.co.indocyber.bmm.dto.MobSptDto;
import id.co.indocyber.bmm.entity.MobDriver;
import id.co.indocyber.bmm.entity.MobSpt;
import id.co.indocyber.bmm.service.MobSptSvc;

@Service("mobSptSvc")
@Transactional
public class MobSptSvcImpl implements MobSptSvc {

	@Autowired
	MobSptDao mobSptDao;
	@Autowired
	MobDriverDao mobDriverDao;
	
	public Boolean init=false;

	@Override
	public HashMap<String, Object> findSpt(String username, String status) {
		
		List<Object[]> listobjSpt = mobSptDao.findSPT(username, status);
		List<MobSptDto> lisMobSptDtos = new ArrayList<MobSptDto>();
		HashMap<String, Object> result = new HashMap<>();
		SimpleDateFormat a = new SimpleDateFormat("dd/MM/yyyy");
		if (listobjSpt.isEmpty()) {
			result.put("status", 0);
			result.put("message", "data tidak ada");
			result.put("object", null);
		} else {
			for (Object[] o : listobjSpt) {
				MobSptDto mobSptDto = new MobSptDto();
				mobSptDto.setNoSppb((Integer) o[0]);
				mobSptDto.setTglPengambilan(a.format((Date) o[13]));
				mobSptDto.setNoSpt((Integer) o[1]);
				mobSptDto.setPhoto((String) o[14]);
				mobSptDto.setFullname((String) o[15]);
				mobSptDto.setTransportirName(((String) o[2]).trim());
				mobSptDto.setDriverName(username);
				mobSptDto.setTruk((String) o[16]);
				mobSptDto.setNoVehicle((String) o[3]);
				mobSptDto.setNoHp((String) o[4]);
				mobSptDto.setSellerName(((String) o[5]));
				String selleradd=(String) o[6];
				String custName=(String)o[7];
				String custAddr=(String) o[8];
				if(selleradd!=null){

					mobSptDto.setSellerAddrs(selleradd.trim());
				}
				if(custName!=null){

					mobSptDto.setCustName(custName.trim());
				}
				if(custAddr!=null){

					mobSptDto.setCustAddrs(custAddr.trim());
				}
				mobSptDto.setItemName((String) o[9]);
				mobSptDto.setStatus(((Character) o[10]).toString());
				mobSptDto.setQty(((BigDecimal) o[12]) == null ? 0d
						: ((BigDecimal) o[12]).doubleValue()/50);
				mobSptDto.setWeight(((BigDecimal) o[12]) == null ? 0d
						: ((BigDecimal) o[12]).doubleValue());
				mobSptDto.setTglPenugasan(a.format((Date) o[13]));
				mobSptDto.setNoContainer((String) o[17]);
				try{
				mobSptDto.setIsTakeAssignment(((BigInteger) o[18])
						.equals(BigInteger.ZERO) ? false : true);
				}catch(Exception e){
					mobSptDto.setIsTakeAssignment(((Integer) o[18])==0 ? false : true);
				}
				lisMobSptDtos.add(mobSptDto);
			}
			result.put("status", 1);
			result.put("message", "success");
			result.put("object", lisMobSptDtos);
		}
		return result;
	}

	@Override
	public HashMap<String, Object> editSpt(Integer sptNo, String longitude,
			String latitude) {
		HashMap<String, Object> result = new HashMap<>();
		
		MobSpt mobSpt = mobSptDao.findOne(sptNo);
		MobDriver mobDriver = mobDriverDao.findOne(mobSpt.getDriverId());
		mobSpt.setTakeAssignmentDate(new Date());
		mobSpt.setTakeAssignmentLatitude(Double.parseDouble(latitude));
		mobSpt.setTakeAssignmentLongitude(Double.parseDouble(longitude));
		// mobSpt.setModified();
		mobSpt.setModifiedDate(new Date());
		mobSpt.setModified(mobDriver.getPhoneNum());
		try {
			mobSptDao.save(mobSpt);
			result.put("status", 1);
			result.put("message", "success");
			result.put("object", null);
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", "gagal update data");
			result.put("object", null);
		}

		return result;
	}

	@Override
	public HashMap<String, Object> tesScheduler(String noSpt) {

		String status="";
		HashMap<String, Object> result = new HashMap<>();
		HttpResponse response;
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(
				"http://103.66.69.18:6026/tokenservices/getresttoken");
		JSONObject jobj = new JSONObject();
		try {
			response = httpClient.execute(request);

			HttpEntity entity = response.getEntity();
			String responseString;
			responseString = EntityUtils.toString(entity);
			jobj = new JSONObject(responseString);
			// HashMap<String, String> data = ;
			HashMap<String, Object> yourHashMap = new Gson().fromJson(
					jobj.get("data").toString(), HashMap.class);
			String a = yourHashMap.get("token").toString();
			try {
				String url="http://103.66.69.18:6026/sptservices/mobspt/list";
				HttpClient client2 = new DefaultHttpClient();
				HttpPost post = new HttpPost(url);

				List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
				urlParameters.add(new BasicNameValuePair("token", a));

				post.setEntity(new UrlEncodedFormEntity(urlParameters));
				
				JSONObject jobj2 = new JSONObject();
				
				response = httpClient.execute(post);
				HttpEntity entity2 = response.getEntity();
				String responseString2 = EntityUtils.toString(entity2);
				
				
				jobj = new JSONObject(responseString2);
				
				
//				HashMap<String, Object> yourHashMap2 = new Gson().fromJson(
//						jobj.get("data").toString(), HashMap.class);
				
				JSONObject obj = jobj.getJSONObject("data");
				JSONArray joar = obj.getJSONArray("data");
				List<ListDataApi> list = new ArrayList<>();
//				try {
//					 list = JsonUtil.mapJsonToListObject(joar, ListDataApi.class);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
				
				for (int i = 0; i < joar.length(); i++) {
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("spt_no")) || joar.getJSONObject(i).get("spt_no").equals("") ) {
						System.err.println("Spt kosong");
						status="spt kosong";
					}else if(noSpt.equals(joar.getJSONObject(i).getString("spt_no"))){
						MobSpt mobSpt = mobSptDao.findSptbyNo(Integer.parseInt(noSpt));
						mobSpt.setStatus(joar.getJSONObject(i).getString("status"));
						status=mobSpt.getStatus();
						mobSptDao.save(mobSpt);
					}else{
						status="spt tidak ditemukan";
					}
					
				}
				if(status.equals("a")){
					result.put("status", 1);
					result.put("message", "sukses");
					result.put("object", status);
				}else{
					result.put("status", 0);
					result.put("message", "error");
					result.put("object", status);
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				result.put("status", 0);
				result.put("message", "eror");
				result.put("object", e.toString());
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				result.put("status", 0);
				result.put("message", "eror");
				result.put("object", e.toString());
				e.printStackTrace();
			}

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			result.put("status", 0);
			result.put("message", "eror");
			result.put("object", e.toString());
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			result.put("status", 0);
			result.put("message", "eror");
			result.put("object", e.toString());
			e.printStackTrace();
		}

		return result;
	}
	
	@Override
	public void getNotification(String noHp){
		
		if(init==false){
			GoogleCredentials credentials;
			try {
				credentials = GoogleCredentials.fromStream(
						//new FileInputStream("C:/Users/User/Downloads/sparepartfactory-4d619-firebase-adminsdk-71jvb-f5547e041b.json"))
						new URL(AttributeParam.hostJson).openStream())
						.createScoped(Lists.newArrayList("https://www.googleapis.com/auth/cloud-platform"));
						FirebaseOptions options = new FirebaseOptions.Builder()
						.setCredentials(credentials)
						.setDatabaseUrl("https://berkahmm-78460.firebaseio.com").build();

				FirebaseApp.initializeApp(options);
				init=true;
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		String listStringToken = mobSptDao.findSPTPhone(noHp);
		List<String> token = new ArrayList<>();
		token.add(listStringToken);
		if(!listStringToken.isEmpty()){
			MulticastMessage messageSales = MulticastMessage
					.builder()
					.setNotification(
							new Notification("BMM", "Ada Spt baru nih"))
					.addAllTokens(token)
					.setAndroidConfig(
							AndroidConfig
									.builder()
									.setNotification(
											AndroidNotification.builder()
													.setColor("#f15a24")
													.build()).build())
													.putData("noHp", noHp)
													.build();
			try {
				BatchResponse response = FirebaseMessaging.getInstance()
						.sendMulticast(messageSales);
				
			} catch (FirebaseMessagingException e) {
				e.printStackTrace();
				
			}
			
		}
	}

}
