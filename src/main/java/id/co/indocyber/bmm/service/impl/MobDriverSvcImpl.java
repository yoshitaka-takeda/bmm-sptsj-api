package id.co.indocyber.bmm.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmm.common.AttributeParam;
import id.co.indocyber.bmm.config.ConfigFunction;
import id.co.indocyber.bmm.dao.MUserDao;
import id.co.indocyber.bmm.dao.MobDriverDao;
import id.co.indocyber.bmm.dao.MobMappingTransportirDriverdDao;
import id.co.indocyber.bmm.dao.MobTransportirDao;
import id.co.indocyber.bmm.dto.UserProfileDto;
import id.co.indocyber.bmm.entity.MUser;
import id.co.indocyber.bmm.entity.MUserPK;
import id.co.indocyber.bmm.entity.MobDriver;
import id.co.indocyber.bmm.service.MobDriverSvc;

@Service("mobDriverSvc")
@Transactional
public class MobDriverSvcImpl implements MobDriverSvc {

	@Autowired
	MobDriverDao mobDriverDao;
	@Autowired
	MUserDao mUserDao;
	@Autowired
	MobTransportirDao mobTransportirDao;
	@Autowired
	MobMappingTransportirDriverdDao mappingTransportirDriverdDao;
	
	
	
	@Override
	public HashMap<String, Object> profileDriver(String email) {
		HashMap<String, Object> result = new HashMap<>();
		List<Object[]> profilObj = mobDriverDao.findUserProfile(email);
		if(profilObj.isEmpty()){
			result.put("status", 0);
			result.put("message", "data tidak ditemukan");
			result.put("object", null);
		}else{
		UserProfileDto userProfileDto = new UserProfileDto();
		for (Object[] o : profilObj) {
			userProfileDto.setPhotoText((String)o[0]);
			userProfileDto.setFullname((String)o[1]);
			userProfileDto.setAddress((String)o[2]);
			userProfileDto.setRtrw((String)o[3]);
			userProfileDto.setVillage((String)o[4]);
			userProfileDto.setSubdistrict((String)o[5]);
			userProfileDto.setDistrict((String)o[6]);
			userProfileDto.setProvince((String)o[7]);
			userProfileDto.setCity((String)o[8]);
			userProfileDto.setZipcode((String)o[9]);
			userProfileDto.setGender((String)o[10]);
			userProfileDto.setNoTelp((String)o[11]);
			userProfileDto.setNoKtp((String)o[12]);
			userProfileDto.setNoRegister((String)o[13]);
			
		}
		result.put("status", 1);
		result.put("message", "sukses");
		result.put("object", userProfileDto);
		
		}
		return result;
	}

	@Override
	public HashMap<String, Object> updateProfile(UserProfileDto userProfileDto,
			String email) {
		HashMap<String, Object> result = new HashMap<>();
		Date now = new Date(); 
		
		MUserPK mUserPK = new MUserPK();
		mUserPK.setEmail(email);
		MUser mUser = mUserDao.findOne(mUserPK);
		MobDriver mobDriver = mobDriverDao.findMobDriverEdit(email);
		//MobMappingTransportirDriver mDriver = mappingTransportirDriverdDao.findOne(mobDriver.getId());
		//MobTransportir mobTransportir = mobTransportirDao.findMobTransportirEdit(email);
		
		if(mUser!=null || mobDriver!=null ){
			ConfigFunction saveImage= new ConfigFunction();
			mUser.setPhotoText(userProfileDto.getPhotoText().toString().equals("")?mUser.getPhotoText():AttributeParam.host+saveImage.SaveBase64toImage(userProfileDto.getPhotoText(),AttributeParam.folderNameUser,1).substring(19).replace("\\", "/"));
			mUser.setFullname(userProfileDto.getFullname().toString().equals("")?mUser.getFullname():userProfileDto.getFullname());
			mUser.setModifiedDate(now);
						
			mobDriver.setAddress(userProfileDto.getAddress().toString().equals("")?mobDriver.getAddress():userProfileDto.getAddress());
			mobDriver.setRtrw(userProfileDto.getRtrw().toString().equals("")?mobDriver.getRtrw():userProfileDto.getRtrw());
			mobDriver.setVillage(userProfileDto.getVillage().toString().equals("")?mobDriver.getVillage():userProfileDto.getVillage());
			mobDriver.setSubDistrict(userProfileDto.getSubdistrict().toString().equals("")?mobDriver.getSubDistrict():userProfileDto.getSubdistrict());
			mobDriver.setDistrict(userProfileDto.getDistrict().toString().equals("")?mobDriver.getDistrict():userProfileDto.getDistrict());
			mobDriver.setProvince(userProfileDto.getProvince().toString().equals("")?mobDriver.getProvince():userProfileDto.getProvince());
			mobDriver.setCity(userProfileDto.getCity().toString().equals("")?mobDriver.getCity():userProfileDto.getCity());
			mobDriver.setZipcode(userProfileDto.getZipcode().toString().equals("")?mobDriver.getZipcode():userProfileDto.getZipcode());
			mobDriver.setGender(userProfileDto.getGender().toString().equals("")?mobDriver.getGender():userProfileDto.getGender());
			mobDriver.setPhoneNum(userProfileDto.getNoTelp().toString().equals("")?mobDriver.getPhoneNum():userProfileDto.getNoTelp());
			mobDriver.setNoKtp(userProfileDto.getNoKtp().toString().equals("")?mobDriver.getNoKtp():userProfileDto.getNoKtp());
			mobDriver.setReferenceCode(userProfileDto.getNoRegister().toString().equals("")?mobDriver.getReferenceCode():userProfileDto.getNoRegister());
			mobDriver.setFullname(userProfileDto.getFullname().toString().equals("")?mobDriver.getFullname():userProfileDto.getFullname());
			mobDriver.setModifiedDate(now);
			
			//mobTransportir.setTransportirName(userProfileDto.getNamaTransportir().toString().equals("")?mobTransportir.getTransportirName():userProfileDto.getNamaTransportir());
			
			try {
				mUserDao.save(mUser);
				mobDriverDao.save(mobDriver);
		//		mappingTransportirDriverdDao.save(mDriver);
				result.put("status", 1);
				result.put("message", "sukses");
				result.put("object", userProfileDto);
			} catch (Exception e) {
				e.printStackTrace();
				result.put("status", 0);
				result.put("message", "gagal simpan");
				result.put("object", userProfileDto);
			}
		}
		
		return result;
	}

}
