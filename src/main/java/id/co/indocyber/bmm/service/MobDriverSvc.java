package id.co.indocyber.bmm.service;


import id.co.indocyber.bmm.dto.UserEditForm;
import id.co.indocyber.bmm.dto.UserProfileDto;

import java.util.HashMap;

public interface MobDriverSvc {

	public HashMap<String, Object> profileDriver(String email);
	public HashMap<String, Object> updateProfile(UserProfileDto userProfileDto,String email);
}
