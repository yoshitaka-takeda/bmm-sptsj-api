package id.co.indocyber.bmm.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmm.dao.MMenuDao;
import id.co.indocyber.bmm.dto.MMenuDto;
import id.co.indocyber.bmm.entity.MMenu;
import id.co.indocyber.bmm.service.MMenuSvc;

@Service("mMenuSvc")
@Transactional
public class MMenuSvcImpl implements MMenuSvc {

	@Autowired
	MMenuDao mMenuDao;
	
	@Override
	public List<MMenuDto> selectMenu() {
		List<MMenu> mList = mMenuDao.findAll();
		List<MMenuDto> menuDtos = new ArrayList<MMenuDto>();
		for (MMenu o : mList) {
			MMenuDto menuDto = new MMenuDto();
			menuDto.setMenuCode(o.getMenuCode());
			menuDto.setMenuName(o.getMenuName());
			menuDto.setMenuDisplayName(o.getMenuDisplayName());
			menuDtos.add(menuDto);
		}
		return menuDtos;
	}

}
