package id.co.indocyber.bmm.service;

import java.util.HashMap;

import id.co.indocyber.bmm.dto.ChangePassDto;
import id.co.indocyber.bmm.dto.LoginDto;

public interface MUserSvc {

	public HashMap<String, Object> login2(LoginDto loginDto);
	public HashMap<String, Object> login(LoginDto loginDto);
	public HashMap<String, Object> loginXml(LoginDto loginDto);
	public HashMap<String, Object> changePassword(ChangePassDto changePassDto);
	public HashMap<String, Object> registerImei(String email, String imei);
	public HashMap<String, Object> getNoHp(String imei);

	
	//public HashMap<String, Object> registerImei(String imei);
}
