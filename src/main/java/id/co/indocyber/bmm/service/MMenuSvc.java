package id.co.indocyber.bmm.service;

import java.util.List;

import id.co.indocyber.bmm.dto.MMenuDto;

public interface MMenuSvc {

	public List<MMenuDto> selectMenu();
}
