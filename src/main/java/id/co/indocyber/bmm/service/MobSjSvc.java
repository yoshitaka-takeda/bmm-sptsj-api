package id.co.indocyber.bmm.service;

import id.co.indocyber.bmm.dto.FormSjEditDto;
import id.co.indocyber.bmm.dto.SjDepartingDto;

import java.util.HashMap;

public interface MobSjSvc {
	
	public HashMap<String, Object> getSj(String userName, String status);
	public HashMap<String, Object> searchSj(String userName, String dateTo, String dateFrom);
	//public HashMap<String, Object> editSj(Integer sjNo, String latitude,String longitude);
	public HashMap<String, Object> editSjAndGoodReceive(FormSjEditDto editDto);
	//public HashMap<String, Object> editSjTransit(FormSjEditDto editDto);
	//public HashMap<String, Object> inputGambar(FormSjEditDto editDto);
	public HashMap<String, Object> pinValidation(String pin,Integer sjNo);
	public HashMap<String, Object> departing(SjDepartingDto departingDto);


}
