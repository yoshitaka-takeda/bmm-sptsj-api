package id.co.indocyber.bmm.common;

public class AttributeParam {

	public static final String hostToken="http://api.berkahmm.com:8097/zirconite/tokenservices/getresttoken";
	public static final String hostUpdateSJ="http://api.berkahmm.com:8097/zirconite/sptservices/mobsj/update";
	public static final String folderNameAttachment = "attachment/attachment";
	public static final String folderNameSignature="signature/signature";
	public static final String folderNameUser="user/user";
	public static final String host="http://mobile.berkahmm.com:8080";
	public static final String hostJson="http://mobile.berkahmm.com:8080/bmm-asset/berkahmm-78460-firebase-adminsdk-xwo8d-d9e22c8826.json";
}
