package id.co.indocyber.bmm.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the mob_mapping_transportir_driver database table.
 * 
 */
@Entity
@Table(name="mob_mapping_transportir_driver")
@NamedQuery(name="MobMappingTransportirDriver.findAll", query="SELECT m FROM MobMappingTransportirDriver m")
public class MobMappingTransportirDriver implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Column(name="transportir_code")
	private String transportirCode;

	@Id
	@Column(name="driver_id")
	private int driverId;

	@Column(name="created")
	private String created;

	@Column(name="created_date")
	private Date createdDate;

	@Column(name="is_active")
	private byte isActive;

	@Column(name="modified")
	private String modified;

	@Column(name="modified_date")
	private Date modifiedDate;

	public String getTransportirCode() {
		return transportirCode;
	}

	public void setTransportirCode(String transportirCode) {
		this.transportirCode = transportirCode;
	}

	public int getDriverId() {
		return driverId;
	}

	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}

	public MobMappingTransportirDriver() {
	}

	public String getCreated() {
		return this.created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public String getModified() {
		return this.modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

}