package id.co.indocyber.bmm.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the m_user_role database table.
 * 
 */
@Entity
@Table(name="m_user_role")
@NamedQuery(name="MUserRole.findAll", query="SELECT m FROM MUserRole m")
@IdClass(MUserRolePK.class)
public class MUserRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="email")
	private String email;

	@Id
	@Column(name="role_id")
	private int roleId;

	@Column(name="created_by")
	private String createdBy;

	@Column(name="created_date")
	private Date createdDate;

	@Column(name="is_active")
	private byte isActive;

	@Column(name="modified_by")
	private String modifiedBy;

	@Column(name="modified_date")
	private Date modifiedDate;

	@Column(name="module_apps")
	private String moduleApps;

	public MUserRole() {
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public int getRoleId() {
		return roleId;
	}



	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}



	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getModuleApps() {
		return this.moduleApps;
	}

	public void setModuleApps(String moduleApps) {
		this.moduleApps = moduleApps;
	}

}