package id.co.indocyber.bmm.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the mob_goods_receive_attachment database table.
 * 
 */
@Entity
@Table(name="mob_goods_receive_attachment")
@NamedQuery(name="MobGoodsReceiveAttachment.findAll", query="SELECT m FROM MobGoodsReceiveAttachment m")
public class MobGoodsReceiveAttachment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Integer id;

	@Column(name="created")
	private String created;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_date")
	private Date createdDate;

	@Lob
	@Column(name="document_blob")
	private String documentBlob;

	@Column(name="document_path")
	private String documentPath;

	@Lob
	private String notes;

	@Column(name="sj_no")
	private Integer sjNo;

	public MobGoodsReceiveAttachment() {
	}

	
	public MobGoodsReceiveAttachment( String created, Date createdDate,
			String documentBlob, String documentPath, String notes, Integer sjNo) {
		this.created = created;
		this.createdDate = createdDate;
		this.documentBlob = documentBlob;
		this.documentPath = documentPath;
		this.notes = notes;
		this.sjNo = sjNo;
	}


	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCreated() {
		return this.created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDocumentBlob() {
		return this.documentBlob;
	}

	public void setDocumentBlob(String documentBlob) {
		this.documentBlob = documentBlob;
	}

	public String getDocumentPath() {
		return this.documentPath;
	}

	public void setDocumentPath(String documentPath) {
		this.documentPath = documentPath;
	}

	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Integer getSjNo() {
		return this.sjNo;
	}

	public void setSjNo(Integer sjNo) {
		this.sjNo = sjNo;
	}

}