package id.co.indocyber.bmm.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the stg_spt database table.
 * 
 */
@Entity
@Table(name="stg_spt")
@NamedQuery(name="StgSpt.findAll", query="SELECT s FROM StgSpt s")
public class StgSpt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int docentry;

	private int baseentry;

	private int baselinenum;

	private int baseno;

	private String basetype;

	@Temporal(TemporalType.TIMESTAMP)
	private Date batchdate;

	private String batchnum;

	private String cancelled;

	private String cardcode;

	private String cardname;

	private String containerno;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createdate;

	private String createdby;

	private short createtime;

	private String creator;

	private String datasource;

	@Temporal(TemporalType.TIMESTAMP)
	private Date docdate;

	private int docnum;

	private String docstatus;

	private String driver;

	private String handwritten;

	private short instance;

	private String itemcode;

	private String itemname;

	private String ktpfname;

	private String ktpfpath;

	private int loginst;

	private String nopol;

	private String nosj;

	private int period;

	private String phone;

	private BigDecimal qty;

	private BigDecimal qtysppb;

	@Lob
	private String remark;

	private String requeststatus;

	private int series;

	private String shipto;

	private String status;

	private String transferred;

	private String truck;

	private String uobject;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedate;

	private short updatetime;

	private int usersign;

	private BigDecimal weightbruto;

	private BigDecimal weightnetto;

	public StgSpt() {
	}

	public int getDocentry() {
		return this.docentry;
	}

	public void setDocentry(int docentry) {
		this.docentry = docentry;
	}

	public int getBaseentry() {
		return this.baseentry;
	}

	public void setBaseentry(int baseentry) {
		this.baseentry = baseentry;
	}

	public int getBaselinenum() {
		return this.baselinenum;
	}

	public void setBaselinenum(int baselinenum) {
		this.baselinenum = baselinenum;
	}

	public int getBaseno() {
		return this.baseno;
	}

	public void setBaseno(int baseno) {
		this.baseno = baseno;
	}

	public String getBasetype() {
		return this.basetype;
	}

	public void setBasetype(String basetype) {
		this.basetype = basetype;
	}

	public Date getBatchdate() {
		return this.batchdate;
	}

	public void setBatchdate(Date batchdate) {
		this.batchdate = batchdate;
	}

	public String getBatchnum() {
		return this.batchnum;
	}

	public void setBatchnum(String batchnum) {
		this.batchnum = batchnum;
	}

	public String getCancelled() {
		return this.cancelled;
	}

	public void setCancelled(String cancelled) {
		this.cancelled = cancelled;
	}

	public String getCardcode() {
		return this.cardcode;
	}

	public void setCardcode(String cardcode) {
		this.cardcode = cardcode;
	}

	public String getCardname() {
		return this.cardname;
	}

	public void setCardname(String cardname) {
		this.cardname = cardname;
	}

	public String getContainerno() {
		return this.containerno;
	}

	public void setContainerno(String containerno) {
		this.containerno = containerno;
	}

	public Date getCreatedate() {
		return this.createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public String getCreatedby() {
		return this.createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public short getCreatetime() {
		return this.createtime;
	}

	public void setCreatetime(short createtime) {
		this.createtime = createtime;
	}

	public String getCreator() {
		return this.creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getDatasource() {
		return this.datasource;
	}

	public void setDatasource(String datasource) {
		this.datasource = datasource;
	}

	public Date getDocdate() {
		return this.docdate;
	}

	public void setDocdate(Date docdate) {
		this.docdate = docdate;
	}

	public int getDocnum() {
		return this.docnum;
	}

	public void setDocnum(int docnum) {
		this.docnum = docnum;
	}

	public String getDocstatus() {
		return this.docstatus;
	}

	public void setDocstatus(String docstatus) {
		this.docstatus = docstatus;
	}

	public String getDriver() {
		return this.driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getHandwritten() {
		return this.handwritten;
	}

	public void setHandwritten(String handwritten) {
		this.handwritten = handwritten;
	}

	public short getInstance() {
		return this.instance;
	}

	public void setInstance(short instance) {
		this.instance = instance;
	}

	public String getItemcode() {
		return this.itemcode;
	}

	public void setItemcode(String itemcode) {
		this.itemcode = itemcode;
	}

	public String getItemname() {
		return this.itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}

	public String getKtpfname() {
		return this.ktpfname;
	}

	public void setKtpfname(String ktpfname) {
		this.ktpfname = ktpfname;
	}

	public String getKtpfpath() {
		return this.ktpfpath;
	}

	public void setKtpfpath(String ktpfpath) {
		this.ktpfpath = ktpfpath;
	}

	public int getLoginst() {
		return this.loginst;
	}

	public void setLoginst(int loginst) {
		this.loginst = loginst;
	}

	public String getNopol() {
		return this.nopol;
	}

	public void setNopol(String nopol) {
		this.nopol = nopol;
	}

	public String getNosj() {
		return this.nosj;
	}

	public void setNosj(String nosj) {
		this.nosj = nosj;
	}

	public int getPeriod() {
		return this.period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public BigDecimal getQty() {
		return this.qty;
	}

	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}

	public BigDecimal getQtysppb() {
		return this.qtysppb;
	}

	public void setQtysppb(BigDecimal qtysppb) {
		this.qtysppb = qtysppb;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getRequeststatus() {
		return this.requeststatus;
	}

	public void setRequeststatus(String requeststatus) {
		this.requeststatus = requeststatus;
	}

	public int getSeries() {
		return this.series;
	}

	public void setSeries(int series) {
		this.series = series;
	}

	public String getShipto() {
		return this.shipto;
	}

	public void setShipto(String shipto) {
		this.shipto = shipto;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTransferred() {
		return this.transferred;
	}

	public void setTransferred(String transferred) {
		this.transferred = transferred;
	}

	public String getTruck() {
		return this.truck;
	}

	public void setTruck(String truck) {
		this.truck = truck;
	}

	public String getUobject() {
		return this.uobject;
	}

	public void setUobject(String uobject) {
		this.uobject = uobject;
	}

	public Date getUpdatedate() {
		return this.updatedate;
	}

	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}

	public short getUpdatetime() {
		return this.updatetime;
	}

	public void setUpdatetime(short updatetime) {
		this.updatetime = updatetime;
	}

	public int getUsersign() {
		return this.usersign;
	}

	public void setUsersign(int usersign) {
		this.usersign = usersign;
	}

	public BigDecimal getWeightbruto() {
		return this.weightbruto;
	}

	public void setWeightbruto(BigDecimal weightbruto) {
		this.weightbruto = weightbruto;
	}

	public BigDecimal getWeightnetto() {
		return this.weightnetto;
	}

	public void setWeightnetto(BigDecimal weightnetto) {
		this.weightnetto = weightnetto;
	}

}