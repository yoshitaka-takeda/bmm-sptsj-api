package id.co.indocyber.bmm.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the mob_spt database table.
 * 
 */
@Entity
@Table(name="mob_spt")
@NamedQuery(name="MobSpt.findAll", query="SELECT m FROM MobSpt m")
public class MobSpt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="spt_no")
	private Integer sptNo;

	@Column(name="address_ship_from")
	private String addressShipFrom;

	@Column(name="address_ship_to")
	private String addressShipTo;

	@Column(name="container_no")
	private String containerNo;

	@Column(name="created")
	private String created;

	@Column(name="created_date")
	private Date createdDate;

	@Column(name="cust_code")
	private String custCode;

	@Column(name="cust_name")
	private String custName;

	@Column(name="driver_id")
	private Integer driverId;

	@Column(name="item_code")
	private String itemCode;

	@Column(name="item_name")
	private String itemName;

	@Column(name="ktp_picture")
	private String ktpPicture;

	@Column(name="modified")
	private String modified;

	@Column(name="modified_date")
	private Date modifiedDate;

	@Column(name="phone")
	private String phone;

	@Column(name="qty")
	private Double qty;

	@Column(name="qty_sppb")
	private Double qtySppb;

	@Column(name="reference_code")
	private String referenceCode;

	@Column(name="sj_no")
	private String sjNo;

	@Column(name="sppb_key")
	private Integer sppbKey;

	@Column(name="sppb_no")
	private Integer sppbNo;

	@Column(name="spt_date")
	private Date sptDate;

	@Column(name="spt_expired_date")
	private Date sptExpiredDate;

	@Column(name="status")
	private String status;

	@Column(name="take_assignment_by")
	private String takeAssignmentBy;

	@Column(name="take_assignment_date")
	private Date takeAssignmentDate;

	@Column(name="take_assignment_latitude")
	private Double takeAssignmentLatitude;

	@Column(name="take_assignment_longitude")
	private Double takeAssignmentLongitude;

	@Column(name="transportir_code")
	private String transportirCode;

	@Column(name="transportir_name")
	private String transportirName;

	@Column(name="vehicle_no")
	private String vehicleNo;

	@Column(name="vehicle_type")
	private Integer vehicleType;

	@Column(name="weight_bruto")
	private Double weightBruto;

	@Column(name="weight_netto")
	private Double weightNetto;
	
	@Column(name="is_transit")
	private Boolean isTransit;

	public MobSpt() {
	}

	
	
	public Boolean getIsTransit() {
		return isTransit;
	}



	public void setIsTransit(Boolean isTransit) {
		this.isTransit = isTransit;
	}



	public Integer getSptNo() {
		return this.sptNo;
	}

	public void setSptNo(Integer sptNo) {
		this.sptNo = sptNo;
	}

	public String getAddressShipFrom() {
		return this.addressShipFrom;
	}

	public void setAddressShipFrom(String addressShipFrom) {
		this.addressShipFrom = addressShipFrom;
	}

	public String getAddressShipTo() {
		return this.addressShipTo;
	}

	public void setAddressShipTo(String addressShipTo) {
		this.addressShipTo = addressShipTo;
	}

	public String getContainerNo() {
		return this.containerNo;
	}

	public void setContainerNo(String containerNo) {
		this.containerNo = containerNo;
	}

	public String getCreated() {
		return this.created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCustCode() {
		return this.custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getCustName() {
		return this.custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public Integer getDriverId() {
		return this.driverId;
	}

	public void setDriverId(Integer driverId) {
		this.driverId = driverId;
	}

	public String getItemCode() {
		return this.itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return this.itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getKtpPicture() {
		return this.ktpPicture;
	}

	public void setKtpPicture(String ktpPicture) {
		this.ktpPicture = ktpPicture;
	}

	public String getModified() {
		return this.modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Double getQty() {
		return this.qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public Double getQtySppb() {
		return this.qtySppb;
	}

	public void setQtySppb(Double qtySppb) {
		this.qtySppb = qtySppb;
	}

	public String getReferenceCode() {
		return this.referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public String getSjNo() {
		return this.sjNo;
	}

	public void setSjNo(String sjNo) {
		this.sjNo = sjNo;
	}

	public Integer getSppbKey() {
		return this.sppbKey;
	}

	public void setSppbKey(Integer sppbKey) {
		this.sppbKey = sppbKey;
	}

	public Integer getSppbNo() {
		return this.sppbNo;
	}

	public void setSppbNo(Integer sppbNo) {
		this.sppbNo = sppbNo;
	}

	public Date getSptDate() {
		return this.sptDate;
	}

	public void setSptDate(Date sptDate) {
		this.sptDate = sptDate;
	}

	public Date getSptExpiredDate() {
		return this.sptExpiredDate;
	}

	public void setSptExpiredDate(Date sptExpiredDate) {
		this.sptExpiredDate = sptExpiredDate;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTakeAssignmentBy() {
		return this.takeAssignmentBy;
	}

	public void setTakeAssignmentBy(String takeAssignmentBy) {
		this.takeAssignmentBy = takeAssignmentBy;
	}

	public Date getTakeAssignmentDate() {
		return this.takeAssignmentDate;
	}

	public void setTakeAssignmentDate(Date takeAssignmentDate) {
		this.takeAssignmentDate = takeAssignmentDate;
	}

	public Double getTakeAssignmentLatitude() {
		return this.takeAssignmentLatitude;
	}

	public void setTakeAssignmentLatitude(Double takeAssignmentLatitude) {
		this.takeAssignmentLatitude = takeAssignmentLatitude;
	}

	public Double getTakeAssignmentLongitude() {
		return this.takeAssignmentLongitude;
	}

	public void setTakeAssignmentLongitude(Double takeAssignmentLongitude) {
		this.takeAssignmentLongitude = takeAssignmentLongitude;
	}

	public String getTransportirCode() {
		return this.transportirCode;
	}

	public void setTransportirCode(String transportirCode) {
		this.transportirCode = transportirCode;
	}

	public String getTransportirName() {
		return this.transportirName;
	}

	public void setTransportirName(String transportirName) {
		this.transportirName = transportirName;
	}

	public String getVehicleNo() {
		return this.vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public Integer getVehicleType() {
		return this.vehicleType;
	}

	public void setVehicleType(Integer vehicleType) {
		this.vehicleType = vehicleType;
	}

	public Double getWeightBruto() {
		return this.weightBruto;
	}

	public void setWeightBruto(Double weightBruto) {
		this.weightBruto = weightBruto;
	}

	public Double getWeightNetto() {
		return this.weightNetto;
	}

	public void setWeightNetto(Double weightNetto) {
		this.weightNetto = weightNetto;
	}

}