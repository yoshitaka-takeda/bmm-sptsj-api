package id.co.indocyber.bmm.entity;

import java.io.Serializable;

/**
 * The primary key class for the m_user_role database table.
 * 
 */
public class MUserRolePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private String email;

	private int roleId;

	public MUserRolePK() {
	}
	public String getEmail() {
		return this.email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getRoleId() {
		return this.roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof MUserRolePK)) {
			return false;
		}
		MUserRolePK castOther = (MUserRolePK)other;
		return 
			this.email.equals(castOther.email)
			&& (this.roleId == castOther.roleId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.email.hashCode();
		hash = hash * prime + this.roleId;
		
		return hash;
	}
}