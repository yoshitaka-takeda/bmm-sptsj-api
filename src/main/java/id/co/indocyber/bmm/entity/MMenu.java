package id.co.indocyber.bmm.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the m_menu database table.
 * 
 */
@Entity
@Table(name="m_menu")
@NamedQuery(name="MMenu.findAll", query="SELECT m FROM MMenu m")
@IdClass(MMenuPK.class)
public class MMenu implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="menu_code")
	private String menuCode;

	@Column(name="app_type")
	private String appType;

	@Column(name="created")
	private String created;

	@Column(name="created_date")
	private Date createdDate;

	@Column(name="description")
	private String description;

	@Column(name="is_active")
	private byte isActive;

	@Column(name="menu_action")
	private String menuAction;

	@Column(name="menu_display_name")
	private String menuDisplayName;

	@Column(name="menu_link")
	private String menuLink;

	@Column(name="menu_logo")
	private String menuLogo;

	@Column(name="menu_name")
	private String menuName;

	@Column(name="modified")
	private String modified;

	@Column(name="modified_date")
	private Date modifiedDate;

	@Column(name="parent_menu_code")
	private String parentMenuCode;

	@Column(name="position")
	private int position;

	public MMenu() {
	}

	public String getMenuCode() {
		return this.menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	public String getAppType() {
		return this.appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}

	public String getCreated() {
		return this.created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public String getMenuAction() {
		return this.menuAction;
	}

	public void setMenuAction(String menuAction) {
		this.menuAction = menuAction;
	}

	public String getMenuDisplayName() {
		return this.menuDisplayName;
	}

	public void setMenuDisplayName(String menuDisplayName) {
		this.menuDisplayName = menuDisplayName;
	}

	public String getMenuLink() {
		return this.menuLink;
	}

	public void setMenuLink(String menuLink) {
		this.menuLink = menuLink;
	}

	public String getMenuLogo() {
		return this.menuLogo;
	}

	public void setMenuLogo(String menuLogo) {
		this.menuLogo = menuLogo;
	}

	public String getMenuName() {
		return this.menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getModified() {
		return this.modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getParentMenuCode() {
		return this.parentMenuCode;
	}

	public void setParentMenuCode(String parentMenuCode) {
		this.parentMenuCode = parentMenuCode;
	}

	public int getPosition() {
		return this.position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

}