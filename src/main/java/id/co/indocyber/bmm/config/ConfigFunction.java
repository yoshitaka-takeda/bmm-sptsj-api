package id.co.indocyber.bmm.config;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Date;

public class ConfigFunction {

	public String SaveBase64toImage(String base64, String folderName, int seq){
		String pathFile = "";
		try {
		String extension ="jpg";
		//String[] strings = base64.split(",");
		byte[] decodedBytes = Base64.getDecoder().decode(base64.getBytes(StandardCharsets.UTF_8));
//		byte[] decodedBytes = Base64.getDecoder().decode(base64);
//		pathFile = "D:\\tesImage"+new Date().getDate()+new Date().getMonth()+new Date().getYear()+new Date().getTime()+"."+extension;
		pathFile = "/opt/Tomcat/webapps/bmm-asset/"+folderName+seq+new Date().getDate()+new Date().getMonth()+new Date().getYear()+new Date().getTime()+"."+extension;
		FileOutputStream imageOutFile = null;
		
			imageOutFile = new FileOutputStream(pathFile);
			imageOutFile.write(decodedBytes);
			imageOutFile.close();
		} catch (FileNotFoundException e) {
			System.err.println(e.toString());
			pathFile = "/opt/Tomcat/webapps/bmm-asset/download.gif";

		} catch (IOException e) {
			System.err.println(e.toString());
			pathFile = "/opt/Tomcat/webapps/bmm-asset/download.gif";
		}
		
		return pathFile;
	}
}
