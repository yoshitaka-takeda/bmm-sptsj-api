package id.co.indocyber.bmm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;


@Configuration
@EnableScheduling
public class SchedulerConfig {

	@Bean
	public Scheduler schedule(){
		return new Scheduler();
	}
	
}
