package id.co.indocyber.bmm.main;

import id.co.indocyber.bmm.config.Scheduler;
import id.co.indocyber.bmm.config.SchedulerConfig;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	@SuppressWarnings({ "unused", "resource" })
	public static void main(String[] args) {
		//ApplicationContext ctx = new ClassPathXmlApplicationContext("/META-INF/spring/app-config.xml");
		System.out.println("Sukses");
		AbstractApplicationContext  context = new AnnotationConfigApplicationContext(SchedulerConfig.class);
	}

}
