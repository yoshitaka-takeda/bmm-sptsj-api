package id.co.indocyber.bmm.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ListDataApi implements Serializable{
	
		
		private String sptNo;
		private Date sptDate;
		private Date sptExpDate,createdDate;
		private Integer sppbNo;
		private Integer sppbKey;
		private String custCode;
		private String custName, addressSHipTo,transportirCode, transportirName, addressShipFrom,itemCode,itemName,
		vehicleNo,phone,containerNo,sjNo,status,created;
		private Double qtySppb,weightNetto,weightBruto;
		
		
		public String getSptNo() {
			return sptNo;
		}
		public void setSptNo(String sptNo) {
			this.sptNo = sptNo;
		}
		public Date getSptDate() {
			return sptDate;
		}
		public void setSptDate(Date sptDate) {
			this.sptDate = sptDate;
		}
		public Date getSptExpDate() {
			return sptExpDate;
		}
		public void setSptExpDate(Date sptExpDate) {
			this.sptExpDate = sptExpDate;
		}
		public Date getCreatedDate() {
			return createdDate;
		}
		public void setCreatedDate(Date createdDate) {
			this.createdDate = createdDate;
		}
		public Integer getSppbNo() {
			return sppbNo;
		}
		public void setSppbNo(Integer sppbNo) {
			this.sppbNo = sppbNo;
		}
		public Integer getSppbKey() {
			return sppbKey;
		}
		public void setSppbKey(Integer sppbKey) {
			this.sppbKey = sppbKey;
		}
		public String getCustCode() {
			return custCode;
		}
		public void setCustCode(String custCode) {
			this.custCode = custCode;
		}
		public String getCustName() {
			return custName;
		}
		public void setCustName(String custName) {
			this.custName = custName;
		}
		public String getAddressSHipTo() {
			return addressSHipTo;
		}
		public void setAddressSHipTo(String addressSHipTo) {
			this.addressSHipTo = addressSHipTo;
		}
		public String getTransportirCode() {
			return transportirCode;
		}
		public void setTransportirCode(String transportirCode) {
			this.transportirCode = transportirCode;
		}
		public String getTransportirName() {
			return transportirName;
		}
		public void setTransportirName(String transportirName) {
			this.transportirName = transportirName;
		}
		public String getAddressShipFrom() {
			return addressShipFrom;
		}
		public void setAddressShipFrom(String addressShipFrom) {
			this.addressShipFrom = addressShipFrom;
		}
		public String getItemCode() {
			return itemCode;
		}
		public void setItemCode(String itemCode) {
			this.itemCode = itemCode;
		}
		public String getItemName() {
			return itemName;
		}
		public void setItemName(String itemName) {
			this.itemName = itemName;
		}
		public String getVehicleNo() {
			return vehicleNo;
		}
		public void setVehicleNo(String vehicleNo) {
			this.vehicleNo = vehicleNo;
		}
		public String getPhone() {
			return phone;
		}
		public void setPhone(String phone) {
			this.phone = phone;
		}
		public String getContainerNo() {
			return containerNo;
		}
		public void setContainerNo(String containerNo) {
			this.containerNo = containerNo;
		}
		public String getSjNo() {
			return sjNo;
		}
		public void setSjNo(String sjNo) {
			this.sjNo = sjNo;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getCreated() {
			return created;
		}
		public void setCreated(String created) {
			this.created = created;
		}
		public Double getQtySppb() {
			return qtySppb;
		}
		public void setQtySppb(Double qtySppb) {
			this.qtySppb = qtySppb;
		}
		public Double getWeightNetto() {
			return weightNetto;
		}
		public void setWeightNetto(Double weightNetto) {
			this.weightNetto = weightNetto;
		}
		public Double getWeightBruto() {
			return weightBruto;
		}
		public void setWeightBruto(Double weightBruto) {
			this.weightBruto = weightBruto;
		}
	}


	
	

