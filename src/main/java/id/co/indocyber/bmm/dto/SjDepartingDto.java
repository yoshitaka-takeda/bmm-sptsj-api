package id.co.indocyber.bmm.dto;

public class SjDepartingDto {

	private String departingLatitude, departingLongitude, userName;
	private Integer sjNo;
	public String getDepartingLatitude() {
		return departingLatitude;
	}
	public void setDepartingLatitude(String departingLatitude) {
		this.departingLatitude = departingLatitude;
	}
	public String getDepartingLongitude() {
		return departingLongitude;
	}
	public void setDepartingLongitude(String departingLongitude) {
		this.departingLongitude = departingLongitude;
	}

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Integer getSjNo() {
		return sjNo;
	}
	public void setSjNo(Integer sjNo) {
		this.sjNo = sjNo;
	}
	
	
}
