package id.co.indocyber.bmm.dto;


import java.util.Date;

import javax.persistence.Column;

public class MobSjDto {
	private int sjNo;

	private String addressShipFrom;

	private String status;

	private String addressShipTo;

	private String containerNo;

	private String custCode;

	private String custName;

	private String departingFromBy;

	private Date departingFromDate;

	private Double departingFromLatitude;

	private Double departingFromLongitude;

	private String destinationBy;
	
	private Date destinationDate;

	private Double destinationLatitude;
	
	private Double destinationLongitude;

	private int driverId;

	private String itemCode;

	private String itemName;

	private String phone;

	private Double qty;

	private Double qtySppb;

	private String segel;

	private String spmNo;

	private int sptNo;

	private int transportirId;

	private String transportirName;

	private String vehicleNo;

	private int vehicleType;
	
	private Integer pin;
	
	private Date createdDate;

	private String created;
	
	private String modified;
	
	private Date modifiedDate;
	
	private Double firtsDestinationlatitude;

	private Double firtsDestinationlongitude;
	
	private Date firtsDestinationDate;

	private String firtsDestinationBy;
	
	private Boolean isTransit;
	
	private Double transitLatitude;
	
	private Double transitLongitude;
	
	private Date transitDate;
	
	private String transitBy;

	
	public String getDestinationBy() {
		return destinationBy;
	}

	public void setDestinationBy(String destinationBy) {
		this.destinationBy = destinationBy;
	}

	public Date getDestinationDate() {
		return destinationDate;
	}

	public void setDestinationDate(Date destinationDate) {
		this.destinationDate = destinationDate;
	}

	public int getSjNo() {
		return sjNo;
	}

	public void setSjNo(int sjNo) {
		this.sjNo = sjNo;
	}

	public String getAddressShipFrom() {
		return addressShipFrom;
	}

	public void setAddressShipFrom(String addressShipFrom) {
		this.addressShipFrom = addressShipFrom;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAddressShipTo() {
		return addressShipTo;
	}

	public void setAddressShipTo(String addressShipTo) {
		this.addressShipTo = addressShipTo;
	}

	public String getContainerNo() {
		return containerNo;
	}

	public void setContainerNo(String containerNo) {
		this.containerNo = containerNo;
	}

	public String getCustCode() {
		return custCode;
	}

	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getDepartingFromBy() {
		return departingFromBy;
	}

	public void setDepartingFromBy(String departingFromBy) {
		this.departingFromBy = departingFromBy;
	}

	public Date getDepartingFromDate() {
		return departingFromDate;
	}

	public void setDepartingFromDate(Date departingFromDate) {
		this.departingFromDate = departingFromDate;
	}

	public Double getDepartingFromLatitude() {
		return departingFromLatitude;
	}

	public void setDepartingFromLatitude(Double departingFromLatitude) {
		this.departingFromLatitude = departingFromLatitude;
	}

	public Double getDepartingFromLongitude() {
		return departingFromLongitude;
	}

	public void setDepartingFromLongitude(Double departingFromLongitude) {
		this.departingFromLongitude = departingFromLongitude;
	}

	public Double getDestinationLatitude() {
		return destinationLatitude;
	}

	public void setDestinationLatitude(Double destinationLatitude) {
		this.destinationLatitude = destinationLatitude;
	}

	public Double getDestinationLongitude() {
		return destinationLongitude;
	}

	public void setDestinationLongitude(Double destinationLongitude) {
		this.destinationLongitude = destinationLongitude;
	}

	public int getDriverId() {
		return driverId;
	}

	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public Double getQtySppb() {
		return qtySppb;
	}

	public void setQtySppb(Double qtySppb) {
		this.qtySppb = qtySppb;
	}


	public String getSegel() {
		return segel;
	}

	public void setSegel(String segel) {
		this.segel = segel;
	}

	public String getSpmNo() {
		return spmNo;
	}

	public void setSpmNo(String spmNo) {
		this.spmNo = spmNo;
	}

	public int getSptNo() {
		return sptNo;
	}

	public void setSptNo(int sptNo) {
		this.sptNo = sptNo;
	}

	public int getTransportirId() {
		return transportirId;
	}

	public void setTransportirId(int transportirId) {
		this.transportirId = transportirId;
	}

	public String getTransportirName() {
		return transportirName;
	}

	public void setTransportirName(String transportirName) {
		this.transportirName = transportirName;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public int getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(int vehicleType) {
		this.vehicleType = vehicleType;
	}

	public Integer getPin() {
		return pin;
	}

	public void setPin(Integer pin) {
		this.pin = pin;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getModified() {
		return modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Double getFirtsDestinationlatitude() {
		return firtsDestinationlatitude;
	}

	public void setFirtsDestinationlatitude(Double firtsDestinationlatitude) {
		this.firtsDestinationlatitude = firtsDestinationlatitude;
	}

	public Double getFirtsDestinationlongitude() {
		return firtsDestinationlongitude;
	}

	public void setFirtsDestinationlongitude(Double firtsDestinationlongitude) {
		this.firtsDestinationlongitude = firtsDestinationlongitude;
	}

	public Date getFirtsDestinationDate() {
		return firtsDestinationDate;
	}

	public void setFirtsDestinationDate(Date firtsDestinationDate) {
		this.firtsDestinationDate = firtsDestinationDate;
	}

	public String getFirtsDestinationBy() {
		return firtsDestinationBy;
	}

	public void setFirtsDestinationBy(String firtsDestinationBy) {
		this.firtsDestinationBy = firtsDestinationBy;
	}

	public Boolean getIsTransit() {
		return isTransit;
	}

	public void setIsTransit(Boolean isTransit) {
		this.isTransit = isTransit;
	}

	public Double getTransitLatitude() {
		return transitLatitude;
	}

	public void setTransitLatitude(Double transitLatitude) {
		this.transitLatitude = transitLatitude;
	}

	public Double getTransitLongitude() {
		return transitLongitude;
	}

	public void setTransitLongitude(Double transitLongitude) {
		this.transitLongitude = transitLongitude;
	}

	public Date getTransitDate() {
		return transitDate;
	}

	public void setTransitDate(Date transitDate) {
		this.transitDate = transitDate;
	}

	public String getTransitBy() {
		return transitBy;
	}

	public void setTransitBy(String transitBy) {
		this.transitBy = transitBy;
	}	
	
	
}
