package id.co.indocyber.bmm.dto;

import java.util.Date;

import org.joda.time.DateTime;

public class MobSptDto {

	private String photo,fullname,transportirName,driverName,truk,
	noVehicle,noHp,sellerName,sellerAddrs,custName,custAddrs,itemName,status,noContainer;
	
	private String tglPenugasan,tglPengambilan;
	
	private Integer noSppb,noSpt;
	
	private Double Weight,qty;
	private Boolean isTakeAssignment;
	
	
	

	
	public Boolean getIsTakeAssignment() {
		return isTakeAssignment;
	}

	public void setIsTakeAssignment(Boolean isTakeAssignment) {
		this.isTakeAssignment = isTakeAssignment;
	}

	public String getNoContainer() {
		return noContainer;
	}

	public void setNoContainer(String noContainer) {
		this.noContainer = noContainer;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getTransportirName() {
		return transportirName;
	}

	public void setTransportirName(String transportirName) {
		this.transportirName = transportirName;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getTruk() {
		return truk;
	}

	public void setTruk(String truk) {
		this.truk = truk;
	}

	public String getNoVehicle() {
		return noVehicle;
	}

	public void setNoVehicle(String noVehicle) {
		this.noVehicle = noVehicle;
	}

	public String getNoHp() {
		return noHp;
	}

	public void setNoHp(String noHp) {
		this.noHp = noHp;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String noSeller) {
		this.sellerName = noSeller;
	}

	public String getSellerAddrs() {
		return sellerAddrs;
	}

	public void setSellerAddrs(String sellerAddrs) {
		this.sellerAddrs = sellerAddrs;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustAddrs() {
		return custAddrs;
	}

	public void setCustAddrs(String custAddrs) {
		this.custAddrs = custAddrs;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getNoSppb() {
		return noSppb;
	}

	public void setNoSppb(Integer noSppb) {
		this.noSppb = noSppb;
	}

	public Integer getNoSpt() {
		return noSpt;
	}

	public void setNoSpt(Integer noSpt) {
		this.noSpt = noSpt;
	}

	public String getTglPenugasan() {
		return tglPenugasan;
	}

	public void setTglPenugasan(String tglPenugasan) {
		this.tglPenugasan = tglPenugasan;
	}

	public String getTglPengambilan() {
		return tglPengambilan;
	}

	public void setTglPengambilan(String tglPengambilan) {
		this.tglPengambilan = tglPengambilan;
	}

	public Double getWeight() {
		return Weight;
	}

	public void setWeight(Double weight) {
		Weight = weight;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}





	
	
}
