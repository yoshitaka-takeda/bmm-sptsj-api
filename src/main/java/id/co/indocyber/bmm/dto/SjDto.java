package id.co.indocyber.bmm.dto;


public class SjDto {

	private Integer noSj;
	private String tgl;
	private String customer;
	private Integer noSpt;
	private String addressShipTo;
	private String noVehicle;
	private Integer noSppb;
	private String noSegel;
	private String itemName;
	private Double weight;
	private Double qty;
	private String keterangan;
	private Boolean isTransit,isDepart;
	private Double qtyReceive;
	private String notes;
	private String containerNo;
	
	
	
	public String getContainerNo() {
		return containerNo;
	}
	public void setContainerNo(String containerNo) {
		this.containerNo = containerNo;
	}
	public Boolean getIsDepart() {
		return isDepart;
	}
	public void setIsDepart(Boolean isDepart) {
		this.isDepart = isDepart;
	}
	public Double getQtyReceive() {
		return qtyReceive;
	}
	public void setQtyReceive(Double qtyReceive) {
		this.qtyReceive = qtyReceive;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Boolean getIsTransit() {
		return isTransit;
	}
	public void setIsTransit(Boolean isTransit) {
		this.isTransit = isTransit;
	}
	public Integer getNoSj() {
		return noSj;
	}
	public void setNoSj(Integer noSj) {
		this.noSj = noSj;
	}
	public String getTgl() {
		return tgl;
	}
	public void setTgl(String tgl) {
		this.tgl = tgl;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public Integer getNoSpt() {
		return noSpt;
	}
	public void setNoSpt(Integer noSpt) {
		this.noSpt = noSpt;
	}
	public String getAddressShipTo() {
		return addressShipTo;
	}
	public void setAddressShipTo(String addressShipTo) {
		this.addressShipTo = addressShipTo;
	}
	public String getNoVehicle() {
		return noVehicle;
	}
	public void setNoVehicle(String noVehicle) {
		this.noVehicle = noVehicle;
	}
	public Integer getNoSppb() {
		return noSppb;
	}
	public void setNoSppb(Integer noSppb) {
		this.noSppb = noSppb;
	}
	public String getNoSegel() {
		return noSegel;
	}
	public void setNoSegel(String noSegel) {
		this.noSegel = noSegel;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	
	
}
