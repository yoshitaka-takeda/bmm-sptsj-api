package id.co.indocyber.bmm.dto;

import java.util.Date;
import java.util.List;

public class FormSjEditDto {

	private Integer noSj,qtyReceive;
	private String userNameDriver, destinationLatitude, destinationLongitude,notes,signature, recipientName;
	private List<String> photoDoc;


	public String getRecipientName() {
		return recipientName;
	}


	public void setRecipientName(String recipientName) {
		this.recipientName = recipientName;
	}


	public String getUserNameDriver() {
		return userNameDriver;
	}


	public void setUserNameDriver(String userNameDriver) {
		this.userNameDriver = userNameDriver;
	}


	public List<String> getPhotoDoc() {
		return photoDoc;
	}


	public void setPhotoDoc(List<String> photoDoc) {
		this.photoDoc = photoDoc;
	}


	public Integer getQtyReceive() {
		return qtyReceive;
	}
	
	
	public String getSignature() {
		return signature;
	}


	public void setSignature(String signature) {
		this.signature = signature;
	}


	public void setQtyReceive(Integer qtyReceive) {
		this.qtyReceive = qtyReceive;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Integer getNoSj() {
		return noSj;
	}
	public void setNoSj(Integer noSj) {
		this.noSj = noSj;
	}
	
	public String getDestinationLatitude() {
		return destinationLatitude;
	}
	public void setDestinationLatitude(String destinationLatitude) {
		this.destinationLatitude = destinationLatitude;
	}
	public String getDestinationLongitude() {
		return destinationLongitude;
	}
	public void setDestinationLongitude(String destinationLongitude) {
		this.destinationLongitude = destinationLongitude;
	}
	
}
