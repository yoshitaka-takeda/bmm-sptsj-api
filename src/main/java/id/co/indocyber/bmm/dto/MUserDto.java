package id.co.indocyber.bmm.dto;

import java.util.Date;

import javax.persistence.Column;

public class MUserDto {

	private String email;

	private String createdBy;

	private Date createdDate;
	
	private String fullname;

	private String imei;

	private Boolean isActive;

	private String modifiedBy;

	private Date modifiedDate;

	private String nipeg;

	private String password;

	private int personId;

	private String phoneNum;

	private String photoText;

	private String pswdDefault;

	private String referenceCode;

	private String token;

	private String username;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getNipeg() {
		return nipeg;
	}

	public void setNipeg(String nipeg) {
		this.nipeg = nipeg;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getPersonId() {
		return personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getPhotoText() {
		return photoText;
	}

	public void setPhotoText(String photoText) {
		this.photoText = photoText;
	}

	public String getPswdDefault() {
		return pswdDefault;
	}

	public void setPswdDefault(String pswdDefault) {
		this.pswdDefault = pswdDefault;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	
}
