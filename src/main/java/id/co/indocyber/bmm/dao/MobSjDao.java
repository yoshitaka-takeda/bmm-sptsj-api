package id.co.indocyber.bmm.dao;

import java.util.Date;
import java.util.List;

import id.co.indocyber.bmm.entity.MobSj;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobSjDao extends JpaRepository<MobSj, Integer> {

	@Query(nativeQuery=true,value="select a.sj_no,d.spt_date,a.cust_name,d.spt_no,a.address_ship_from, a.vehicle_no, "
			+ "d.sppb_no,a.segel1,a.segel8,a.item_name,d.weight_netto,a.qty,d.is_transit, "
			+ "if(a.departing_from_latitude is null and a.departing_from_longitude is null,false,true),a.container_no "
			+ "from mob_sj a "
			+ "left join mob_driver b "
			+ "on a.driver_id=b.id "
			+ "left join m_user c "
			+ "on c.reference_code=b.id "
			+ "left join mob_spt d "
			+ "on a.spt_no=d.spt_no "
			+ "where c.username=:username "
			+ "and a.`status`=:status "
			+ "and a.destination_latitude is null "
			+ "and a.destination_longitude is null "
			+ "and a.first_destination_latitude is null "
			+ "and a.first_destination_longitude is null "
			+ "order by a.modified_date desc, a.sj_no desc")
	public List<Object[]> findSJ(@Param("username") String username,@Param("status") String status);
	
	@Query(nativeQuery=true,value="select a.sj_no,d.spt_date,a.cust_name,d.spt_no,a.address_ship_from, a.vehicle_no, "
			+ "d.sppb_no,a.segel1,a.segel8,a.item_name,d.weight_netto,a.qty,d.is_transit, e.qty_receive,e.notes, "
			+ "if(a.departing_from_latitude is null and a.departing_from_longitude is null,false,true),a.container_no "
			+ "from mob_sj a "
			+ "join mob_goods_receive f "
			+ "on a.sj_no=f.sj_no "
			+ "left join mob_driver b "
			+ "on f.driver_id=b.id "
			+ "left join m_user c "
			+ "on c.reference_code=b.id "
			+ "left join mob_spt d "
			+ "on a.spt_no=d.spt_no "
			+ "left join mob_goods_receive e "
			+ "on a.sj_no=e.sj_no "
			+ "where c.username=:username "
			+ "and a.destination_latitude is not null "
			+ "and a.destination_longitude is not null "
			+ "and a.created_date between :dateFrom and :dateTo "
			+ "and a.status = 'c' "
			+ "order by a.destination_date desc")
	public List<Object[]> findSJWithDate(@Param("username") String username,
			@Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo);
	
	@Query("select a, b.isTransit "
			+ "from MobSj a, MobSpt b where a.sptNo=b.sptNo and a.sjNo=:cari")
	public List<Object[]> findSJWithNo(@Param("cari") Integer cari);
	
	@Query(nativeQuery=true,value="select a.sj_no,d.spt_date,a.cust_name,d.spt_no,a.address_ship_from, a.vehicle_no, "
			+ "d.sppb_no,a.segel1,a.segel8,a.item_name,d.weight_netto,a.qty,false, "
			+ "if(a.transit_latitude is null and a.transit_longitude is null,false,true),a.container_no  "
			+ "from mob_sj a "
			+ "join mob_goods_receive e "
			+ "on a.sj_no=e.sj_no "
			+ "left join mob_driver b "
			+ "on e.driver_id=b.id "
			+ "left join m_user c "
			+ "on c.reference_code=b.id "
			+ "left join mob_spt d "
			+ "on a.spt_no=d.spt_no "
			+ "where c.username=:username "
			+ "and a.`status`='R' "
			+ "or a.`status`=:status "
			+ "and a.destination_latitude is null "
			+ "and a.destination_longitude is null "
			+ "order by a.modified_date desc, a.sj_no desc")
	public List<Object[]> findSJDriver2(@Param("username") String username,@Param("status") String status);
}
