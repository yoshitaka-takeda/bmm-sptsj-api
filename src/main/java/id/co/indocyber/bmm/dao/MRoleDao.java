package id.co.indocyber.bmm.dao;

import id.co.indocyber.bmm.entity.MRole;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MRoleDao extends JpaRepository<MRole, Integer> {

}
