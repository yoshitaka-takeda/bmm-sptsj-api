package id.co.indocyber.bmm.dao;

import java.util.List;

import id.co.indocyber.bmm.entity.MobGoodsReceiveAttachment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobGoodReceiveAttachmentDao extends JpaRepository<MobGoodsReceiveAttachment, Integer>{

	@Query("select Max(a.id) from MobGoodsReceiveAttachment a")
	public Integer findMaxId();
	
}
