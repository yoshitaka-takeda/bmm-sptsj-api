package id.co.indocyber.bmm.dao;

import id.co.indocyber.bmm.entity.MobGoodsReceive;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobGoodReceiveDao extends JpaRepository<MobGoodsReceive, Integer>{

	@Query("select a from MobGoodsReceive a "
			+ "where a.sjNo=:sjNo")
	public MobGoodsReceive findGoodReceivebySjno(@Param("sjNo") Integer sjNo);
}
