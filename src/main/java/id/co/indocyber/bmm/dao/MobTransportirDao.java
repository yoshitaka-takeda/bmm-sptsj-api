package id.co.indocyber.bmm.dao;

import id.co.indocyber.bmm.entity.MobTransportir;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobTransportirDao extends JpaRepository<MobTransportir, Integer> {

	@Query("select d "
			+ "from MUser a,MobDriver b, MobMappingTransportirDriver c, MobTransportir d "
			+ "where a.referenceCode=b.id and b.id=c.driverId and c.transportirCode=d.code "
			+ "and a.email=:cari")
	public MobTransportir findMobTransportirEdit(@Param("cari") String cari);
}
