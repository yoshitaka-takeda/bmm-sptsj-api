package id.co.indocyber.bmm.dao;

import id.co.indocyber.bmm.entity.MobMappingTransportirDriver;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MobMappingTransportirDriverdDao extends JpaRepository<MobMappingTransportirDriver, Integer>{

}
