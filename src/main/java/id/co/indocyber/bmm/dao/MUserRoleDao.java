package id.co.indocyber.bmm.dao;

import id.co.indocyber.bmm.entity.MUserRole;
import id.co.indocyber.bmm.entity.MUserRolePK;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MUserRoleDao extends JpaRepository<MUserRole, MUserRolePK>{

}
