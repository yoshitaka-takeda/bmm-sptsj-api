package id.co.indocyber.bmm.dao;


import id.co.indocyber.bmm.entity.MMenu;
import id.co.indocyber.bmm.entity.MMenuPK;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MMenuDao extends JpaRepository<MMenu, MMenuPK> {

}
