package id.co.indocyber.bmm.dao;

import java.util.List;

import id.co.indocyber.bmm.entity.MobDriver;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobDriverDao extends JpaRepository<MobDriver, Integer>{

	@Query("select a.photoText,b.fullname,b.address, "
			+ "b.rtrw,b.village,b.subDistrict,b.district,b.province,b.city,b.zipcode, "
			+ "b.gender, b.phoneNum, b.noKtp, b.referenceCode "
			+ "from MUser a,MobDriver b "
			+ "where a.referenceCode=b.id "
			+ "and a.email=:cari")
	public List<Object[]> findUserProfile(@Param("cari") String cari);
	
	@Query("select b "
			+ "from MUser a,MobDriver b "
			+ "where a.referenceCode=b.id "
			+ "and a.email=:cari")
	public MobDriver findMobDriverEdit(@Param("cari") String cari);
	
	@Query("select b "
			+ "from MUser a,MobDriver b, MobMappingTransportirDriver c, MobTransportir d "
			+ "where a.referenceCode=b.id "
			+ "and a.username=:cari")
	public MobDriver findMobDriverbyUsername(@Param("cari") String cari);
}
