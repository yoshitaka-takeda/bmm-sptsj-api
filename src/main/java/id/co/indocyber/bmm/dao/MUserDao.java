package id.co.indocyber.bmm.dao;

import java.util.List;

import id.co.indocyber.bmm.entity.MUser;
import id.co.indocyber.bmm.entity.MUserPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MUserDao extends JpaRepository<MUser, MUserPK>{

	@Query("select a,b.roleId,a.personId,c.roleName,a.fullname,"
			+ "a.email,a.token,a.username,a.photoText, a.isActive, "
			+ "b.isActive, c.isActive, d.isActive,e.isActive "
			+ "from MUser a, MUserRole b, MRole c, MobMappingTransportirDriver d, MobTransportir e "
			+ "where a.email=b.email and b.roleId=c.id and a.referenceCode = d.driverId "
			+ "and d.transportirCode = e.code "
			+ "and (a.email=:cari or a.username=:cari) and (a.password=:pass or a.pswdDefault=:pass)")
	public List<Object[]> findUserLogina(@Param("cari") String cari, @Param("pass") String pass);
	
	@Query("select a.imei from MUser a where a.imei=:imei")
	public List<String> listIMei(@Param("imei") String imei);
	
	@Query("select a from MUser a where a.username=:username or a.email=:username")
	public MUser getUserByUsername(@Param("username") String username);
	
	@Query("select b.phoneNum from MUser a, MobDriver b where a.referenceCode=b.id and a.imei=:imei ")
	public String getPhoneNum(@Param("imei") String imei);
	
}
