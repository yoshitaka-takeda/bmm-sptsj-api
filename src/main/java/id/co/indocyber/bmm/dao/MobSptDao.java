package id.co.indocyber.bmm.dao;

import java.util.List;

import id.co.indocyber.bmm.entity.MobSpt;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobSptDao extends JpaRepository<MobSpt, Integer> {

	@Query(nativeQuery=true,value="select a.sppb_no,a.spt_no,a.transportir_name,a.vehicle_no, "
			+ "a.phone,a.ship_from,a.address_ship_from, "
			+ "a.cust_name,a.address_ship_to,a.item_name, "
			+ "a.`status`,a.weight_bruto,a.qty,a.spt_date, c.photo_text,b.fullname,e.vehicle_type_name,a.container_no,"
			+ "if(a.take_assignment_latitude is null and a.take_assignment_longitude is null,false,true) "
			+ "from mob_spt a "
			+ "left join mob_driver b "
			+ "on a.driver_id=b.id "
			+ "left join m_user c "
			+ "on c.reference_code=b.id "
			+ "left join mob_vehicle d "
			+ "on a.vehicle_no=d.vehicle_no "
			+ "left join m_vehicle_type e "
			+ "on a.vehicle_type = e.id "
			+ "where c.username=:username "
			+ "and a.`status`=:status or a.`status`= 'a' "
			+ "order by a.spt_no desc")
	public List<Object[]> findSPT(@Param("username") String username,@Param("status") String status);
	
	@Query("select a from MobSpt a where a.sptNo=:sptNo")
	public MobSpt findSptbyNo(@Param("sptNo") Integer sptNo);
	
	@Query(nativeQuery=true,value="select c.token "
			+ "from m_user c "
			+ "where c.phone_num=:phone ")
	public String findSPTPhone(@Param("phone") String phone);
	
	
	
}
